//
//  demoUtils.m
//  cgeDemo
//
//  Created by WangYang on 15/9/6.
//  Copyright (c) 2015年 wysaid. All rights reserved.
//

#import "demoUtils.h"
#import <Photos/Photos.h>

UIImage* loadImageCallback(const char* name, void* arg) {
    NSString* filename = [NSString stringWithUTF8String:name];
    return [UIImage imageNamed:filename];
}

void loadImageOKCallback(UIImage* img, void* arg) {
    
}

@implementation DemoUtils

+ (void)saveVideo :(NSURL*)videoURL completionBlock:(void (^)(BOOL, NSURL *url, NSError *))block {
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:videoURL];
        
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (success) {
            
                PHFetchOptions *fetchoptions = [[PHFetchOptions alloc] init];
                fetchoptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
                fetchoptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeVideo];
                
                // After uploading we fetch the PHAsset for most recent video and then get its current location url
                PHAsset *phAsset = [[PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeVideo options:fetchoptions] lastObject];
                
                [[PHImageManager defaultManager] requestAVAssetForVideo:phAsset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info) {
                    NSURL *url = (NSURL *)[[(AVURLAsset *)avAsset URL] filePathURL];
                    block(success, url, error);
                }];
            } else {
               block(success, nil, error);
            }
     }];
}

+ (void)saveImage:(UIImage *)image completionBlock:(void (^)(BOOL, NSURL *url, NSError *))block {
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetChangeRequest creationRequestForAssetFromImage:image];
      
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (success) {
                
                PHFetchOptions *fetchoptions = [[PHFetchOptions alloc] init];
                fetchoptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
                fetchoptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
                
                // After uploading we fetch the PHAsset for most recent image and then get its current location url
                PHAsset *phAsset = [[PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchoptions] lastObject];
                 
                [[PHImageManager defaultManager] requestImageDataForAsset:phAsset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                    
                    if ([info objectForKey:@"PHImageFileURLKey"]) {
                        NSURL *path = [info objectForKey:@"PHImageFileURLKey"];
                        block(success, path, error);
                    }
                 }];
            } else {
              block(success, nil, error);
            }
    }];
}

@end
