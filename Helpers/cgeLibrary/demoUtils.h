//
//  demoUtils.h
//  cgeDemo
//
//  Created by WangYang on 15/9/6.
//  Copyright (c) 2015年 wysaid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

UIImage* loadImageCallback(const char* name, void* arg);
void loadImageOKCallback(UIImage* img, void* arg);

@interface DemoUtils : NSObject

+ (void)saveVideo :(NSURL*)videoURL completionBlock:(void (^)(BOOL, NSURL *url, NSError *))block;
+ (void)saveImage:(UIImage *)image completionBlock:(void (^)(BOOL, NSURL *url, NSError *))block;

@end
