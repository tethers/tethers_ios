//
//  ChooseRoleController.swift
//  Tethers
//
//  Created by Pooja Rana on 29/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ChooseRoleController: UIViewController {
    
    var isSocial = false
    var params = [String: Any]()
    
    // MARK: -  IBOutlet Action 
    @IBAction func backBtnAction(_ sender: Any) {
        self.removeViewController()
    }
    
    func removeViewController(){
        self.view.isUserInteractionEnabled = true
        self.navigationController?.popViewController(animated: false)
        self.view.crossDissolveAnimation(duration: 0.2)
    }
    
    @IBAction func roleSelected(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.view.isUserInteractionEnabled = false

        sender.springAnimation(duration: 0.7) {
            let role = sender.tag == 1 ? "user" : "organizer"
            self.logInApi(role)
        }
    }
    
    // MARK: -  API Call 
    func logInApi(_ role:String) {
        
        params["user_type"] = role
        let apiurl = isSocial ? ConstantModel.shared.api.fbRegister : ConstantModel.shared.api.logIn
        
        ApiCallModel.shared.apiCall(isSilent: false, url:apiurl, parameters: params, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseLoginSignupResponse(response: response, sender: self, callback: { result in
                
                UserDefaultsHandler.pageToShow = ConstantModel.shared.page.Interest
                self.view.crossDissolveAnimation(duration: 0.2)
                self.performSegue(withIdentifier: "showInterestFromRole", sender: nil)
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse, buttonTitles: ["Ok"], style: .alert, action: { _ in
                self.view.crossDissolveAnimation(duration: 0.2)
                self.removeViewController()
            })
        })
    }
}
