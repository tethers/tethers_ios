//
//  ChooseInterestController.swift
//  Tethers
//
//  Created by Signity on 06/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ChooseInterestController: UIViewController {

    @IBOutlet weak var interestCollection: UICollectionView!
    @IBOutlet weak var collectionHgt: NSLayoutConstraint!
    var interests = [InterestModel]()
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        UpdateLocationsModal.shared.determineMyCurrentLocation()

        let arr = ["Cultural", "Education", "Events", "Food", "Movies", "Music", "Party", "Sport", "More"]
        for item in arr {
            let model = InterestModel(interest: item)
            interests.append(model)
        }
        
        let oneCellHgt  = Int(((UIScreen.main.bounds.size.width-50)/3))
        let dividend    = (arr.count/3+(arr.count%3 == 0 ? 0 : 1))  
        collectionHgt.constant = CGFloat(oneCellHgt*(dividend+(arr.count%3 == 0 ? 0 : 1)))
        interestCollection.reloadData()
    }
    
    // MARK: -  UIButton Action 
    @IBAction func updateSelectedInterest(_ sender: Any) {
        let filtered = interests.filter{ $0.isSelected == true }
        if filtered.count > 0 {
            if ConstantModel.shared.currentAddress.lat != "" && ConstantModel.shared.currentAddress.lng != "" {
                self.selectedInterestAPI()
                
            } else {
                self.showAlert(alertMessage: ConstantModel.shared.message.locationNetError, buttonTitles: ["Ok"], style: .alert, action:{ _ in
                    UpdateLocationsModal.shared.determineMyCurrentLocation()
                })
            }
        } else {
            self.showAlert(alertMessage: ConstantModel.shared.message.selectInterest, buttonTitles: ["Ok"], style: .alert, action: nil)
        }
    }
    
    @objc func chooseInterestAction(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: interestCollection)
        let indexPath = interestCollection.indexPathForItem(at: buttonPosition)
        let interest = interests[(indexPath?.row)!]
        interest.isSelected = !interest.isSelected
        
        let imageName = interest.isSelected ? interest.name + "-active" : interest.name
        sender.setImage(UIImage.init(named: imageName), for: .normal)
        sender.springAnimation(duration: 1.5) {
            self.interestCollection.reloadData()
        }
    }

    // MARK: -  API Call 
    func selectedInterestAPI() {
        let params = GetParamModel.shared.getAddInterestParam(long: ConstantModel.shared.currentAddress.lng, lat: ConstantModel.shared.currentAddress.lat, interest: interests)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.addInterest, parameters: params, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                UserDefaultsHandler.pageToShow = ConstantModel.shared.page.TabScreen
                NavigationManager.shared.checkWhereToNav()
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
}
