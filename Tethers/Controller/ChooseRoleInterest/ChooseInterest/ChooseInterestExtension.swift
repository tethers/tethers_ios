//
//  ChooseInterestExtension.swift
//  Tethers
//
//  Created by Pooja Rana on 30/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import Foundation
import UIKit

extension ChooseInterestController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: -  UICollectionView Delegate 
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseInterestCellId", for: indexPath) as? ChooseInterestCell
        cell?.interestImg.addTarget(self, action: #selector(self.chooseInterestAction(_:)), for: .touchUpInside)
        
        let interest = interests[indexPath.row]
        cell?.interestLbl.text = interest.name
        
        let imageName = interest.isSelected ? interest.name + "-active" : interest.name
        cell?.interestImg.setImage(UIImage.init(named: imageName), for: .normal)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let value: CGFloat = ((UIScreen.main.bounds.size.width-50)/3)
        return CGSize(width: value, height: value)
    }
}
