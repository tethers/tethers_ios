//
//  VideoEditor.swift
//  Tethers
//
//  Created by Pooja Rana on 24/01/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit
import AVKit

class VideoEditor: UIViewController {

    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var trimmerView: TrimmerView!
    
    var asset: AVAsset!
    var player: AVPlayer?
    var playbackTimeCheckerTimer: Timer?
    var callback : ((URL) -> Void)?
    var media : MediaModel!
    var isComeFromMyPost = false

    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        trimmerView.handleColor = UIColor.black
        trimmerView.mainColor = UIColor.yellow
        
        trimmerView.asset = asset
        trimmerView.delegate = self
        addVideoPlayer(with: asset, playerView: playerView)
    }
    
    // MARK: -  AVPlayer 
    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        
        NotificationCenter.default.addObserver(self, selector: #selector(VideoEditor.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
        
        player?.play()
        startPlaybackTimeChecker()
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = trimmerView.startTime {
            player?.seek(to: startTime)
        }
    }
    
    func startPlaybackTimeChecker() {
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(VideoEditor.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        if playbackTimeCheckerTimer?.isValid ?? false {
            playbackTimeCheckerTimer?.invalidate()
            playbackTimeCheckerTimer = nil
        }
    }
    
    @objc func onPlaybackTimeChecker() {
        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime, let player = player else {
            return
        }
        
        let playBackTime = player.currentTime()
        trimmerView.seek(to: playBackTime)
        
        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            trimmerView.seek(to: startTime)
        }
    }
    
    // MARK: -  UIButton Action 
    @IBAction func backBtnAction(_ sender: Any?) {
        self.player = nil
        stopPlaybackTimeChecker()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cropVideo() {
        
        player?.pause()
        EventManager.showloader()
        
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
        
        let url = documentDirectory.appendingPathComponent(Date().getDatefor(format: DateFormats.uploadMediaDateFormat) + ".mp4")
        exportSession.outputURL = url
        exportSession.outputFileType = AVFileType.mp4
        
        let timeRange = CMTimeRange(start: trimmerView.startTime!, end: trimmerView.endTime!)
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                 DispatchQueue.main.async {
                    EventManager.hideloader()
                    if self.isComeFromMyPost{
                        self.callback?(url)
                        self.backBtnAction(nil)
                    }else{
                        //push to my post
                        self.performSegue(withIdentifier: "addPostFromVideo", sender: self.media)
                    }
                 }
                
            default:
                DispatchQueue.main.async { EventManager.hideloader() }
                print(exportSession.error?.localizedDescription ?? "Cannot Export")
            }
        }
    }
    
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addPostFromVideo"{
            let nextVC = segue.destination as! AddPostController
            nextVC.postModel.media = [MediaModel]()
            nextVC.postModel.media?.append(self.media)
        }
    }
}

extension VideoEditor: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        player?.play()
        startPlaybackTimeChecker()
    }
    
    func didChangePositionBar(_ playerTime: CMTime) {
        stopPlaybackTimeChecker()
        player?.pause()
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
}
