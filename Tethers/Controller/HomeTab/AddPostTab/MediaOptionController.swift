//
//  MediaOptionController.swift
//  Tethers
//
//  Created by Pooja Rana on 01/11/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import AVKit

class MediaOptionController: UIViewController {
    
    //Refrences
    var media = MediaModel()
    
    // MARK: -  ViewLifeCycle 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: -  UIButton Action 
    @IBAction func openCameraAction(_ sender: UIButton) {
        sender.springAnimation(duration: 0.5) {
            self.performSegue(withIdentifier: "showCamera", sender: nil)
        }
    }
    
    @IBAction func openGalleryAction(_ sender: UIButton) {
        sender.springAnimation(duration: 0.7) {
            self.openImagePicker(sourceType: .photoLibrary, forMedia: ["public.image", "public.movie"])
        }
        
        didfetchMedia = { picker, model in
            self.media = model
            picker.dismiss(animated: false) {
                if model.isImage{
                    self.performSegue(withIdentifier: "galleryFilter", sender: model)
                }else{
                    self.performSegue(withIdentifier: "videoFilter", sender: model)
                }
            }
        }
    }   
    
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "galleryFilter" {
            let nextVC   = segue.destination as! ImageEditor
            nextVC.thumb = media.thumb
            nextVC.media = media
        }else if segue.identifier == "videoFilter" {
            let nextVC = segue.destination as! VideoEditor
            nextVC.asset = AVAsset.init(url: media.mediaUrl!)
            nextVC.media = media
        }
    }
}
