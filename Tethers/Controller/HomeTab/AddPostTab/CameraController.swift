//
//  CameraController.swift
//  Tethers
//
//  Created by Pooja Rana on 24/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import cge

class CameraController: UIViewController {
    
    @IBOutlet weak var glkView: GLKView!
    var myCameraViewHandler: CGECameraViewHandler?
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLbl: UILabel!
    
    @IBOutlet weak var filterCollection: UICollectionView!
    
    @IBOutlet weak var selectedGalleryView: UIView!
    @IBOutlet weak var capturedImageForShow: UIImageView!
    @IBOutlet weak var capturedCountLbl: UILabel!

    @IBOutlet weak var progressBar: CircularProgressBar!
    @IBOutlet weak var captureBtn: UIButton!
    
    var savedMedia = [MediaModel]()
    var movieUrl: URL!
    var mediaLimit = ConstantModel.shared.mediaLimit
    var callback : (([MediaModel]) -> Void)?
    var media = MediaModel()

    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        //Add Long press gesture for recording video
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        captureBtn.addGestureRecognizer(longPress)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
        if myCameraViewHandler == nil { setUpCameraAndRefreshView() }
    }
    
    // MARK: -  SetUp Camera & Refresh UIComponents 
    func setUpCameraAndRefreshView() {
        
        savedMedia.removeAll()
        capturedCountLbl.text = String(format: "%d", savedMedia.count)
            
        movieUrl = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/Movie.mp4")!

        myCameraViewHandler = CGECameraViewHandler.init(glkView: glkView)
        if (myCameraViewHandler?.setupCamera(AVCaptureSession.Preset.vga640x480.rawValue, cameraPosition: .back, isFrontCameraMirrored: false, authorizationFailed: {
            self.showAlert(alertMessage: ConstantModel.shared.message.cameraError1, buttonTitles: ["OK"], style: .alert, action: nil)

        }))! {
            myCameraViewHandler?.cameraDevice().startCameraCapture()
            
        } else {
            self.showAlert(alertMessage: ConstantModel.shared.message.cameraError2, buttonTitles: ["OK"], style: .alert, action: nil)
        }
        
        CGESharedGLContext.globalSyncProcessingQueue {
            CGESharedGLContext.useGlobalGLContext()
        }
        myCameraViewHandler?.fitViewSizeKeepRatio(true)
        
        //Set to the max resolution for taking photos.
        myCameraViewHandler?.cameraRecorder.setPictureHighResolution(true)
        cgeSetLoadImageCallback(loadImageCallback, loadImageOKCallback, nil)
        
        //Hide Gallery view and progress bar & show informatory message which instruct how to save video
        selectedGalleryView.alpha = 0.0
        
        self.progressBar.foregroundLayer.removeAllAnimations()
        blinkWithInfo(message: ConstantModel.shared.message.videoRecordInfo)
    }
    
    func blinkWithInfo(message: String) {
        DispatchQueue.main.async {
            self.infoView.alpha = 0.0
            self.infoLbl.text   = message
            self.infoView.fadeInFadeOut(0.5, delay: 0.0, alpha: 1.0 ,completion: { success in
                self.infoView.fadeInFadeOut(1.0, delay: 0.0, alpha: 0.0, completion: nil)
            })
        }
    }
    
    func refreshThumb()  {
        DispatchQueue.main.async {
            //If first media is captured then show gallery with animation
            if self.selectedGalleryView.alpha == 0.0 {
                self.selectedGalleryView.fadeInFadeOut(0.2, delay: 0.0, alpha: 1.0 ,completion: { success in
                    self.selectedGalleryView.alpha = 1.0
                    self.changeThumbImage()
                })
            } else {
                self.changeThumbImage()
            }
        }
    }
    
    func changeThumbImage()  {
        //Check if captured media is image or video(show thumb) and increase media counter as well
        if let img = savedMedia.last?.thumb {
            capturedImageForShow.image = img
            
        } else {
            selectedGalleryView.alpha = 0.0
        }
        capturedCountLbl.text = String(format: "%d", savedMedia.count)
    }
    
    // MARK: -  UIButton Action 
    @IBAction func backBtnAction(_ sender: Any) {
        self.isModal() ? self.dismiss(animated: true, completion: nil) : (self.tabBarController?.selectedIndex = 1)
    }
    
    
    @IBAction func openGallary(_ sender: UIButton) {
        sender.springAnimation(duration: 0.7) {
            self.openImagePicker(sourceType: .photoLibrary, forMedia: ["public.image", "public.movie"])
        }
        
        didfetchMedia = { picker, model in
            self.media = model
            picker.dismiss(animated: false) {
                if model.isImage{
                    self.performSegue(withIdentifier: "galleryFilter", sender: model)
                }else{
                    self.performSegue(withIdentifier: "videoFilter", sender: model)
                }
            }
        }
    }
    
    @IBAction func switchCameraAction(_ sender: UIButton) {
        if myCameraViewHandler?.cameraDevice() != nil {
            sender.isSelected = !sender.isSelected
            myCameraViewHandler?.switchCamera(true)
        }
    }
    
    @IBAction func flashAction(_ sender: UIButton) {
        if myCameraViewHandler?.cameraDevice() != nil {
            sender.isSelected = !sender.isSelected
            myCameraViewHandler?.setTorchMode(myCameraViewHandler?.getTorchMode() == .off ? .on : .off)
        }
    }
    
    @objc func longPress(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizer.State.ended {
            
            if myCameraViewHandler?.cameraDevice() != nil {
                //Cannot capture more than 5
                if savedMedia.count <= mediaLimit-1 {
                    if (myCameraViewHandler?.isRecording())! {
                        saveVideo()
                        
                    } else {
                        blinkWithInfo(message: "Recording...")
                        progressBar.setProgress(to: 1, forDuration: 10, withAnimation: true, success: {
                            //Record video upto 10 sec only
                            self.saveVideo()
                        })
                        myCameraViewHandler?.startRecording(movieUrl, size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
                    }
                } else {
                    self.showAlert(alertMessage: ConstantModel.shared.message.mediaLimitError, buttonTitles: ["OK"], style: .alert, action: nil)
                }
            }
        }
    }
    
    @IBAction func takePicture(_ sender: UIButton) {
        //Cannot capture more than 5 and show spring animation on button touch
        sender.springAnimation(duration: 0.4) {
            if self.myCameraViewHandler?.cameraDevice() != nil {
                if self.savedMedia.count <= self.mediaLimit-1 {
                    if (self.myCameraViewHandler?.isRecording())! {
                        self.saveVideo()
                        
                    } else {
                        self.myCameraViewHandler?.takeShot({ image in
                            self.savePhoto(image)
                        })
                    }
                } else {
                    self.showAlert(alertMessage: ConstantModel.shared.message.mediaLimitError, buttonTitles: ["OK"], style: .alert, action: nil)
                }
            }
        }
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        //Clear camera, context and push to upload post
        if myCameraViewHandler?.cameraDevice() != nil {
            myCameraViewHandler?.cameraRecorder.cameraDevice.stopCameraCapture()
            
            //safe clear to avoid memLeaks.
            myCameraViewHandler?.clear()
            myCameraViewHandler = nil
            CGESharedGLContext.clearGlobalGLContext()
        }
        
        if self.isModal() {
            self.callback?(savedMedia)
            self.dismiss(animated: true, completion: nil)
            
        } else {
            self.performSegue(withIdentifier: "uploadPost", sender: nil)
        }
    }
    
    // MARK: -  CGEFrameProcessingDelegate 
    func bufferRequestRGBA() -> Bool {
        return false
    }
    
    // Draw your own content!
    // The content would be shown in realtime, and can be recorded to the video.
    func drawProcResults(_ handler: UnsafeMutableRawPointer!) {
        // unmark below, if you can use cpp. (remember #include "cgeImageHandler.h")
        
        var x: Float = 0
        var dx: Float = 10.0
        glEnable(GLenum(GL_SCISSOR_TEST))
        x += dx
        if x < 0 || x > 500 {
            dx = -dx
        }
        glScissor(GLint(x), 100, 200, 200)
        glClearColor(1, 0.5, 0, 1)
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT))
        glDisable(GLenum(GL_SCISSOR_TEST))
    }
    
    //The realtime buffer for processing. Default format is YUV, and you can change the return value of "bufferRequestRGBA" to recieve buffer of format-RGBA.
    func processingHandle(_ imageBuffer: CVImageBuffer!) -> Bool {
        return false
    }
    
    // MARK: -  Save Videos/Images 
    func saveVideo() {
        let finishBlock: (() -> Void)? = {
            print("End recording...\n")
            
            CGEProcessingContext.mainSyncProcessingQueue({
                //Save video in gallery and take a thumb picture + increase media counter
                DemoUtils.saveVideo(self.movieUrl!, completionBlock: { (success, url, error) in
                    if success {
                        self.myCameraViewHandler?.takeShot({ image in
                            let model = MediaModel(isImg: false, url: url, thumb: image)
                            self.savedMedia.append(model)
                            self.blinkWithInfo(message: "Video Saved")
                            self.refreshThumb()
                        })
                    } else {
                        self.blinkWithInfo(message: error?.localizedDescription ?? ConstantModel.shared.message.videoNotSaveErr)
                    }
                })
            })
        }
        self.progressBar.pauseLayer(layer: self.progressBar.foregroundLayer)
        myCameraViewHandler?.endRecording(finishBlock, withCompressionLevel: 0)
    }
    
    func savePhoto(_ image: UIImage?) {
        DemoUtils.save(image!, completionBlock: { (success, url, error) in
            if success {
                let model = MediaModel(isImg: true, url: url, thumb: image)
                self.savedMedia.append(model)
                self.blinkWithInfo(message: "Image Saved")
                self.refreshThumb()
                
            } else {
                self.blinkWithInfo(message: error?.localizedDescription ?? ConstantModel.shared.message.imageNotSaveErr)
            }
        })
    }
    
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "uploadPost" {
            let nextVC = segue.destination as! AddPostController
            nextVC.postModel.media = savedMedia
        }else if segue.identifier == "galleryFilter" {
            let nextVC   = segue.destination as! ImageEditor
            nextVC.thumb = media.thumb
            nextVC.media = media
        }else if segue.identifier == "videoFilter" {
            let nextVC = segue.destination as! VideoEditor
            nextVC.asset = AVAsset.init(url: media.mediaUrl!)
            nextVC.media = media
        }
    }
}

extension CameraController: UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    // MARK: -  UICollectionView Delegate 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ConstantModel.shared.g_effectConfig.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = filterCollection.dequeueReusableCell(withReuseIdentifier:
            "filterCell", for: indexPath) as? FilterCell
        
        cell?.filterImg.image = UIImage.init(named: String(format:"filter%d", indexPath.row))
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if myCameraViewHandler?.cameraDevice() != nil {
            let config = ConstantModel.shared.g_effectConfig[indexPath.row]
            myCameraViewHandler?.setFilterWithConfig(config)
        }
    }
}
