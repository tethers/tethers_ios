//
//  AddPostController.swift
//  Tethers
//
//  Created by Pooja Rana on 10/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import AVKit

class AddPostController: UIViewController, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var postModel = PostModel()
    
    //Visibility
    @IBOutlet weak var visibilityTxtField: DesignableTextView!
    let visibility       = ["Public", "Private", "Followers"]
    var listView         =  UIPickerView()
    
    @IBOutlet weak var feedTbl: UITableView!
    var tagShownOnce = false
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        listView.delegate = self
        let allListView     = EventManager.inputV(listView, sender: self)
        visibilityTxtField.tintColor = .clear
        visibilityTxtField.inputView = allListView
        
        postModel.postType    = "post"
        postModel.ageCategory = "all"
        
        UpdateLocationsModal.shared.determineMyCurrentLocation()
        getTagsAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: -  UIButton Action 
    @IBAction func backBtnAction(_ sender: Any) {
        if (postModel.media?.count)! > 0 {
            for index in 0...(postModel.media?.count)!-1 {
                EventManager.clearFile(file: (postModel.media![index]).mediaUrl!)
            }
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func postVisibilityAction(_ sender: Any) {
        visibilityTxtField.becomeFirstResponder()
    }
    
    @IBAction func uploadBtnAction(_ sender: Any) {
        self.view.endEditing(true)
        if postModel.postTitle.getTrimmedStr().isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.addTitle, buttonTitles: ["OK"], style: .alert, action: nil)
        } else {
            if ConstantModel.shared.currentAddress.lat != "" && ConstantModel.shared.currentAddress.lng != "" {
                self.postModel.current_lat = ConstantModel.shared.currentAddress.lat
                self.postModel.current_long = ConstantModel.shared.currentAddress.lng

                self.uploadPost()

            } else {
                self.showAlert(alertMessage: ConstantModel.shared.message.locationNetError, buttonTitles: ["Ok"], style: .alert, action:{ _ in
                    UpdateLocationsModal.shared.determineMyCurrentLocation()
                })
            }
        }
    }
    
    func uploadPost() {
        let cell = self.feedTbl.cellForRow(at: IndexPath(row: 1, section: 0)) as! FeedActionItemCell
        postModel.tags  = [TagModel]()
        for tag in cell.tagListView.tagViews {
            postModel.tags?.append(tag.tagObj!)
        }
        
        EventManager.showloader()
        let queue = DispatchQueue(label: "com.company.app.queue", attributes: .concurrent)
        let group = DispatchGroup()
        
        if (postModel.media?.count)! > 0 {
            for index in 0...(postModel.media?.count)!-1 {
                let media = postModel.media![index]
                if media.uploadedUrl == "" {
                    group.enter()
                    queue.async(group: group) {
                        AWSUploader.uploadMediaToAWS(media, uploadFinish: { (status, url) in
                            if status { media.uploadedUrl = url }
                            group.leave()
                        })
                    }
                }
            }
        }
        
        group.notify(queue: queue) {
            DispatchQueue.main.async {
                EventManager.hideloader()
                self.uploadPostAPI()
            }
        }
    }
    
    @IBAction func openCameraAction(_ sender: UIButton) {
        if (postModel.media?.count)! >= ConstantModel.shared.mediaLimit {
            self.showAlert(alertMessage: ConstantModel.shared.message.mediaLimitError, buttonTitles: ["OK"], style: .alert, action: nil)
            
        } else {
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let cameraVC = storyBoard.instantiateViewController(withIdentifier: "cameraTab") as? CameraController
            cameraVC?.mediaLimit = ConstantModel.shared.mediaLimit - (postModel.media?.count)!
            cameraVC?.callback = { arr in
                self.postModel.media?.append(contentsOf: arr)
                self.feedTbl.reloadData()
            }
            self.present(cameraVC!, animated: true, completion: nil)
        }
    }
    
    @objc func moreAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.showAlert(alertMessage: ConstantModel.shared.message.pleaseSelect, buttonTitles: ["Delete", "Edit"], style: .actionSheet, action: { option in
            
            let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: self.feedTbl)
            let indexPath      = self.feedTbl.indexPathForRow(at: buttonPosition)
            
            if option == "Delete" {
                let media          = self.postModel.media![(indexPath?.row)! - 2]
                self.showAlert(alertMessage: ConstantModel.shared.message.deleteMedia + (media.isImage ? "photo" : "video"), buttonTitles: ["OK", "Cancel"], style: .alert, action: { title in
                    if title == "OK" {
                        //Delete Selected media
                        let index = self.postModel.media?.index(of: media)
                        self.postModel.media?.remove(at: index!)
                        self.feedTbl.reloadData()
                    }
                })
            } else if option == "Edit" {
                let media          = self.postModel.media![(indexPath?.row)! - 2]
                let identifier = media.isImage ? "imageEditor" : "showEditor"
                self.performSegue(withIdentifier: identifier, sender: indexPath)
            }
        })
    }
    
    @objc func playVideo(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: feedTbl)
        let indexPath      = feedTbl.indexPathForRow(at: buttonPosition)
        let media          = postModel.media![(indexPath?.row)! - 2]

        if let url = media.mediaUrl {
            let player         = AVPlayer(url: url)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    @IBAction func openImageGallery(_ sender: Any) {
        openMediaPicker(true)
    }
    
    @IBAction func openLocationAction(_ sender: UIButton) {
        self.openPlaces()
        locationCallback = { address in
            
            DispatchQueue.main.async {
                let cell            = self.feedTbl.cellForRow(at: IndexPath(row: 0, section: 0)) as! FeedDescriptionCell
                self.postModel.taggedLocation = address
                cell.peopleLocLbl.text = "- at " + self.postModel.taggedLocation!.address
                self.feedTbl.beginUpdates()
                self.feedTbl.endUpdates()
            }
        }
    }
    
    @IBAction func openPeopleAction(_ sender: Any) {
        showAlert(alertMessage: ConstantModel.shared.message.comingSoon, buttonTitles: ["OK"], style: .alert, action: nil)
    }
    
    @IBAction func openVideoGallery(_ sender: Any) {
        openMediaPicker(false)
    }
    
    func openMediaPicker(_ isImg: Bool) {
        if (postModel.media?.count)! >= ConstantModel.shared.mediaLimit {
            self.showAlert(alertMessage: ConstantModel.shared.message.mediaLimitError, buttonTitles: ["OK"], style: .alert, action: nil)
            
        } else {
            self.openImagePicker(sourceType: .photoLibrary, forMedia: (isImg ? ["public.image"] : ["public.movie"]))
            
            didfetchMedia = { picker, model in
                picker.dismiss(animated: false) {
                    self.postModel.media?.append(model)
                    self.feedTbl.reloadData()
                }
            }
        }
    }
    
    @IBAction func openTagsAction(_ sender: Any) {
        
        if self.postModel.tags != nil && (self.postModel.tags?.count)! > 0 {
            let cell        = self.feedTbl.cellForRow(at: IndexPath(row: 1, section: 0)) as! FeedActionItemCell
            cell.tagViewHgt.constant = cell.tagViewHgt.constant == 0.0 ? 78.0 : 0.0
            
            cell.layoutIfNeeded()
            feedTbl.beginUpdates()
            feedTbl.endUpdates()
            
            if !tagShownOnce {
                tagShownOnce = true
                cell.tagListView.textFont = UIFont.setFontRegular(textFontSize: 14.0)
                cell.tagListView.addTags(self.postModel.tags!)
                cell.tagListView.removePressedHandler = { tag in
                    
                    cell.tagListView.removeTag(tag)
                    let index = self.postModel.tags?.index(of: tag)
                    self.postModel.tags?.remove(at: index!)
                    
                    if cell.tagListView.tagViews.count == 0 {
                        cell.tagViewHgt.constant = 0.0
                        cell.layoutIfNeeded()
                        self.feedTbl.beginUpdates()
                        self.feedTbl.endUpdates()
                    }
                }
            }
        } else {
            self.showAlert(alertMessage: ConstantModel.shared.message.NoTags, buttonTitles: ["OK"], style: .alert, action: nil)
        }
    }
    
    @IBAction func joinSwitchAction(_ sender: UISwitch) {
        postModel.canJoin = sender.isOn
    }
    
    @IBAction func allowCommentLikeAction(_ sender: UISwitch) {
        postModel.allowCommentLike = sender.isOn
    }
    
    // MARK: -  TextView Delegate 
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if visibilityTxtField == textView {
            visibilityTxtField.text = visibility[visibilityTxtField.tag]
            postModel.visibility = visibilityTxtField.text!
            
        } else {
            let pointInTable = textView.convert(textView.bounds.origin, to: self.feedTbl)
            let textViewIndexPath = self.feedTbl.indexPathForRow(at: pointInTable)
            
            if textViewIndexPath?.row == 0 {
                //Post Title
                postModel.postTitle = textView.text
                let cell = feedTbl.cellForRow(at: textViewIndexPath!) as? FeedDescriptionCell
                cell?.layoutIfNeeded()
                
            } else {
                //Caption for Media
                let model     = postModel.media![(textViewIndexPath?.row)! - 2]
                model.caption = textView.text
                let cell = feedTbl.cellForRow(at: textViewIndexPath!) as? FeedMediaCell
                cell?.layoutIfNeeded()
            }
            feedTbl.beginUpdates()
            feedTbl.endUpdates()
        }
    }
    
    @objc func resignTextField(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    // MARK: -  API Call 
    func getTagsAPI() {
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.getTags, parameters: nil, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseGetTagsResponse(response: response, sender: self, callback: { result in
                self.postModel.tags = result as? [TagModel]
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    func uploadPostAPI() {
        let param = GetParamModel.shared.getUploadPostParam(model: self.postModel)
        
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.uploadPost, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                self.showAlert(alertMessage: response!["message"] as? String ?? "", buttonTitles: ["Ok"], style: .alert, action: { _ in
                    self.tabBarController?.selectedIndex = 1
                    self.navigationController?.popToRootViewController(animated: false)
                })
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditor" {
            
            let indexPath   = sender as! IndexPath
            let media       = self.postModel.media![indexPath.row - 2]
            
            let nextVC = segue.destination as! VideoEditor
            nextVC.asset = AVAsset.init(url: media.mediaUrl!)
            nextVC.isComeFromMyPost = true
            nextVC.callback = { url in
                //Replace edited media and reload row
                media.mediaUrl = url
                self.feedTbl.reloadRows(at: [indexPath], with: .none)
            }
        } else if segue.identifier == "imageEditor" {
            let indexPath   = sender as! IndexPath
            let media       = self.postModel.media![indexPath.row - 2]
            
            let nextVC = segue.destination as! ImageEditor
            nextVC.thumb = media.thumb
            nextVC.isComeFromMyPost = true
            nextVC.callback = { url, image in
                //Replace edited media and reload row
                media.mediaUrl = url
                media.thumb = image
                self.feedTbl.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
    @IBAction func ageCategorySelection(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: feedTbl)
        let indexPath      = feedTbl.indexPathForRow(at: buttonPosition)
        let cell           = feedTbl.cellForRow(at: indexPath!) as? AgeRestrictionCell
        
        cell?.allBtn.isSelected  = false
        cell?.below18.isSelected = false
        cell?.above18.isSelected = false
        
        sender.isSelected = !sender.isSelected
        
        postModel.ageCategory = sender.title(for: .normal)!.getTrimmedStr() == "All" ? "all" : (sender.title(for: .normal)!.getTrimmedStr() == "Below 18" ? "below18" : "above18")
    }
}

extension AddPostController: UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (postModel.media?.count)! + 4
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = (cell as? FeedMediaCell) else { return };
        let visibleCells = tableView.visibleCells;
        let minIndex = visibleCells.startIndex;
        if tableView.visibleCells.index(of: cell) == minIndex {
            videoCell.playerView.player?.play();
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = cell as? FeedMediaCell else { return };
        
        videoCell.playerView.player?.pause()
        videoCell.playerView.player = nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // On index 0 --> Feed Description, On index 1 --> Feed Action, On index secondlast --> AgeRestriction, On index last --> FeedJoin
        return indexPath.row == 0 ? addFeedDescriptionCell(indexPath) :
               (indexPath.row == 1 ? addFeedActionItemCell(indexPath) :
               (indexPath.row == (postModel.media?.count)! + 2 ? addAgeRestrictionCell(indexPath) :
               (indexPath.row == (postModel.media?.count)! + 3 ? addFeedJoinCell(indexPath) :
                addFeedMediaCell(indexPath))))
    }
    
    func addFeedDescriptionCell(_ index: IndexPath) -> FeedDescriptionCell {
        let cell = feedTbl.dequeueReusableCell(withIdentifier: "FeedDescriptionCellId") as! FeedDescriptionCell
        cell.feedDescTxt.inputAccessoryView = EventManager.toolBar(isInputAccessory: true, self)
        cell.feedDescTxt.text = postModel.postTitle
        return cell
    }
    
    func addFeedActionItemCell(_ index: IndexPath) -> FeedActionItemCell {
        let cell = feedTbl.dequeueReusableCell(withIdentifier: "FeedActionItemCellId") as! FeedActionItemCell
        return cell
    }
    
    func addFeedMediaCell(_ index: IndexPath) -> FeedMediaCell {
        let cell = feedTbl.dequeueReusableCell(withIdentifier: "FeedMediaCellId") as! FeedMediaCell
        cell.mediaCaptionTxt.inputAccessoryView = EventManager.toolBar(isInputAccessory: true, self)
        cell.moreActionBtn.addTarget(self, action: #selector(moreAction(_:)), for: .touchUpInside)
        cell.videoPlayIcon.addTarget(self, action: #selector(playVideo(_:)), for: .touchUpInside)
        cell.bindData(postModel.media![index.row - 2])
        
        return cell
    }
    
    func addAgeRestrictionCell(_ index: IndexPath) -> UITableViewCell {
        let cell = feedTbl.dequeueReusableCell(withIdentifier: "AgeRestrictionCellId") as! AgeRestrictionCell
        cell.bindData(postModel)
        return cell
    }
    
    func addFeedJoinCell(_ index: IndexPath) -> UITableViewCell {
        let cell = feedTbl.dequeueReusableCell(withIdentifier: "JoinPostCellId") as! JoinPostCell
        cell.bindData(postModel)
        return cell
    }
    
    // MARK: -  UIPicker Delegates 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return visibility.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return visibility[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        visibilityTxtField.tag = row
    }
}
