//
//  ImageEditor.swift
//  Tethers
//
//  Created by Pooja Rana on 18/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import cge

class ImageEditor: UIViewController {
    
    @IBOutlet weak var glkView: GLKView!
    @IBOutlet weak var filterCollection: UICollectionView!
 
    var myImageView: CGEImageViewHandler?
    var thumb: UIImage?
    var callback : ((URL, UIImage) -> Void)?
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        myImageView = CGEImageViewHandler.init(glkView: glkView, with: thumb!)
    }
    
    // MARK: -  UIButton Action 
    @IBAction func backBtnClicked(_ sender: Any) {
        clearSetUp()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        let img = myImageView?.resultImage()
        DemoUtils.save(img, completionBlock: { (success, url, error) in
            DispatchQueue.main.async {
                if success { self.callback?(url!, img!) }
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    func clearSetUp() {
        myImageView?.clear()
        myImageView = nil
    }
}

extension ImageEditor: UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    // MARK: -  UICollectionView Delegate 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ConstantModel.shared.g_effectConfig.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = filterCollection.dequeueReusableCell(withReuseIdentifier:
            "filterCell", for: indexPath) as? FilterCell
        
        cell?.filterImg.image = UIImage.init(named: String(format:"filter%d", indexPath.row))
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let config = ConstantModel.shared.g_effectConfig[indexPath.row]
        myImageView?.setFilterWithConfig(config)
    }
}
