//
//  MyProfileController.swift
//  Tethers
//
//  Created by Pooja Rana on 10/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import SDWebImage

class MyProfileController: UIViewController {

    @IBOutlet weak var myProfileScroll: UIScrollView!
    @IBOutlet weak var coverPic: UIImageView!
    @IBOutlet weak var profilePic: DesignableImageView!
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var followerCountLbl: UILabel!
    @IBOutlet weak var followingCountLbl: UILabel!
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var heightTableView : NSLayoutConstraint!
    @IBOutlet weak var xForCreatedDivider : NSLayoutConstraint!
    @IBOutlet weak var createdButton : UIButton!
    @IBOutlet weak var joiningButton : UIButton!
    
    var model = UserDefaultsHandler.userInfo!
    
    let refreshControl      = UIRefreshControl()
    
    var createdPosts        = [PostModel]()
    var joiningPosts        = [PostModel]()
    
    var isCreatedMoreToRefresh  = true
    var isJoinedMoreToRefresh   = true
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        createdButton.isSelected = true
        joiningButton.isSelected = false
        self.xForCreatedDivider.constant = 0

        refreshControl.addTarget(self, action: #selector(refreshProfileTab), for: .valueChanged)
        myProfileScroll.refreshControl = refreshControl
        
        getProfileAPI(offset: 0)
        getJoiningInterestedPostAPI(offset: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        coverPic.sd_setShowActivityIndicatorView(true)
        coverPic.sd_setIndicatorStyle(.whiteLarge)
        
        profilePic.sd_setShowActivityIndicatorView(true)
        profilePic.sd_setIndicatorStyle(.whiteLarge)
        
        fillProfileDetails()
    }
    
    @objc func refreshProfileTab() {
        createdButton.isSelected ? getProfileAPI(offset: 0) : getJoiningInterestedPostAPI(offset: 0)
    }
    
    func fillProfileDetails() {
        coverPic.downloadImage(url: model.coverPic_url, placeholder: "profile_bg")
        profilePic.downloadImage(url: model.profilePic_url, placeholder: "placeholder")
        
        profileName.text = model.display_name
        if (model.dob == nil || model.dob == "") {
            ageLbl.text = "NA"
            
        } else {
            let calendar = Calendar.current            
            let date1 = model.dob?.getDatefor(format: DateFormats.birthDateFormat)
            let components = calendar.dateComponents([.year], from: date1!, to: Date())
            ageLbl.text = String(format: "%d years", components.year!)
        }
        
        addressLbl.text = (model.address == nil || model.address?.address == "") ? "NA" : (model.address?.address)!
        
        followerCountLbl.text = String(format: "%d", model.followersCount)
        followingCountLbl.text = String(format: "%d", model.followingCount)
    }
    
    // MARK: -  UIButton Action 
    @IBAction func openSettings(_ sender: Any) {
        customPushPop((self.storyboard?.instantiateViewController(withIdentifier: "settingsVC"))!, ispush: true)
    }
    
    @IBAction func createdJoiningAction(_ sender: UIButton) {
        //created - 0, joining - 1
        let isCreated = sender.tag == 100
        createdButton.isSelected = isCreated
        joiningButton.isSelected = !createdButton.isSelected
        
        reloadTableView()
        UIView.animate(withDuration: 0.7, animations: {
            self.xForCreatedDivider.constant = isCreated ? 0 : (self.view.frame.size.width)/2
            self.view.layoutIfNeeded()
        })
    }
    
    //Reload TableView
    func reloadTableView() {
        UIView.transition(with: self.tableView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.tableView.reloadData()
            
        }, completion: { _ in
            self.heightTableView.constant = self.tableView.contentSize.height
            self.view.layoutIfNeeded()
        })
    }
    
    // MARK: -  API Call 
    func getProfileAPI(offset: Int) {
        let param = GetParamModel.shared.getUserParam(offset: offset)!
        ApiCallModel.shared.apiCall(isSilent: offset == 0 ? false : true, url:ConstantModel.shared.api.getUser, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseGetProfileResponse(response: response, sender: self, callback: { result in
                
                let profileData  = result as? [String: Any]
                if offset == 0 {
                    self.createdPosts.removeAll()
                    let profile = profileData!["profile"] as! ProfileModel
                    UserDefaultsHandler.userInfo = profile
                    self.model = profile
                    self.fillProfileDetails()
                }
                
                let moreFeeds = profileData!["createdPosts"] as! [PostModel]
                if moreFeeds.count > 0 {
                    self.createdPosts.append(contentsOf: moreFeeds)
                    self.isCreatedMoreToRefresh  = true
                    self.createdJoiningAction(self.createdButton)
                    
                } else {
                    self.isCreatedMoreToRefresh  = false
                }
                self.refreshControl.endRefreshing()
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
            self.refreshControl.endRefreshing()
        })
    }
    
    func getJoiningInterestedPostAPI(offset: Int) {
        let param = GetParamModel.shared.getUserParam(offset: offset)!
        ApiCallModel.shared.apiCall(isSilent: offset == 0 ? false : true, url:ConstantModel.shared.api.getUserJoiningPosts, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseGetJoiningPostsResponse(response: response, sender: self, callback: { result in
                if offset == 0 { self.joiningPosts.removeAll() }
                let moreFeeds = result as! [PostModel]
                if moreFeeds.count > 0 {
                    self.joiningPosts.append(contentsOf: moreFeeds)
                    self.isJoinedMoreToRefresh  = true
                    self.createdJoiningAction(self.joiningButton)
                    
                } else {
                    self.isJoinedMoreToRefresh   = false
                }
                self.refreshControl.endRefreshing()
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
            self.refreshControl.endRefreshing()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditProfile" {
            let nextVC = segue.destination as? EditProfileViewController
            nextVC?.model = self.model
            
        } else if segue.identifier == "showFollowers" || segue.identifier == "showFollowing" {
            let nextVC = segue.destination as? FollowerFollowingList
            nextVC?.isFollowers = segue.identifier == "showFollowers" ? true : false
            nextVC?.model       = self.model
        }
    }
}

extension MyProfileController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate  {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createdButton.isSelected ? createdPosts.count : joiningPosts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreatedPostsEventsCellId") as! MyPostsEventsCell
        cell.bindData(createdButton.isSelected ? createdPosts[indexPath.row] : joiningPosts[indexPath.row])
        return cell
    }   
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.bounds.size.height) {
            
            if createdButton.isSelected {
                if isCreatedMoreToRefresh == true {
                    getProfileAPI(offset: createdPosts.count)
                }
            } else {
                if isJoinedMoreToRefresh == true {
                    getJoiningInterestedPostAPI(offset: joiningPosts.count)
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.myProfileScroll {
            tableView.isScrollEnabled = (self.myProfileScroll.contentOffset.y >= 200)
        }
        if scrollView == self.tableView {
            self.tableView.isScrollEnabled = (tableView.contentOffset.y > 0)
        }
    }
}
