//
//  FollowerFollowingList.swift
//  Tethers
//
//  Created by Pooja Rana on 21/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class FollowerFollowingList: UIViewController {

    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var userListTbl: UITableView!

    var usersList      = [ProfileModel]()
    var isFollowers    : Bool!
    var isComeFromChat = false
    var model          : ProfileModel!
    
    //Pagination
    var offset             = 0
    var isPageRefresing    = false
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        userListTbl.tableFooterView = UIView.init(frame: .zero)
        headerLbl.text = isFollowers ? "Followers" : "Following"
        if !isComeFromChat{
            getFollowerFollowings()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: -  UIButton Action 
    @objc func followAction(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: userListTbl)
        let indexPath      = userListTbl.indexPathForRow(at: buttonPosition)
        let user           = usersList[(indexPath?.row)!]
        
        if sender.isSelected {
            self.showAlert(alertMessage: "Are you sure, you want to unfollow?", buttonTitles: ["Cancel", "Unfollow"], style: .alert, action: { title in
                if title == "Unfollow" { self.unFollowAPI(user.profile_id!) }
            })
            
        } else {
            followAPI(user.profile_id!)
        }
    }
    
    // MARK: -  API Call 
    func getFollowerFollowings() {
        let url = isFollowers ? ConstantModel.shared.api.getUserFollower : ConstantModel.shared.api.getUserFollowing
        let param = GetParamModel.shared.getListWithPagination(offset: offset)
        
        ApiCallModel.shared.apiCall(isSilent: false, url:url, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseFollowerFollowings(response: response, isFollowers: self.isFollowers, sender: self, callback: { result in
                let moreProfiles = result as! [ProfileModel]
                if moreProfiles.count > 0 {
                    self.usersList.append(contentsOf: moreProfiles)
                    self.userListTbl.reloadData()
                }
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
    
    func followAPI(_ userId: String) {
        
        let param = GetParamModel.shared.followUnFollowParam(userId)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.follow, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in
                
                //My followers --> i can also follow any other user
               self.model.followingCount += 1
                self.showAlert(alertMessage: response!["msg"] as? String ?? "", buttonTitles: ["Ok"], style: .alert, action:{ _ in
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
    
    func unFollowAPI(_ userId: String) {
        
        let param = GetParamModel.shared.followUnFollowParam(userId)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.unFollow, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in
                self.model.followingCount -= 1
                self.showAlert(alertMessage: response!["msg"] as? String ?? "", buttonTitles: ["Ok"], style: .alert, action:{ _ in
                    self.navigationController?.popViewController(animated: true)
                })
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
}

extension FollowerFollowingList: UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.isPageRefresing = false
        return usersList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(format: "FollowFollowingCellId", indexPath.row)) as? FollowFollowingCell
        
        let user = usersList[indexPath.row]
        cell?.userImg.downloadImage(url: user.profilePic_url, placeholder: "placeholder")        
        
        cell?.userName.text = user.display_name
        cell?.userRelation.isHidden = isComeFromChat ? true : false
        cell?.userRelation.addTarget(self, action: #selector(followAction), for: .touchUpInside)
        cell?.userRelation.isSelected = user.followed
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isComeFromChat{
            print("Move to Chat log screen")
            let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
            chatLogController.user = usersList[indexPath.row]
            navigationController?.pushViewController(chatLogController, animated: true)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.userListTbl.contentOffset.y >= (self.userListTbl.contentSize.height - self.userListTbl.bounds.size.height) {
            if isPageRefresing == false {
                // no need to worry about threads because this is always on main thread.
                isPageRefresing = true
                offset = usersList.count
                getFollowerFollowings()
            }
        }
    }
}
