//
//  OthersProfileController.swift
//  
//
//  Created by Pooja Rana on 15/10/18.
//

import UIKit

class OthersProfileController: UIViewController {
    
    @IBOutlet weak var containerScroll: UIScrollView!
    @IBOutlet weak var coverPicImg: UIImageView!
    @IBOutlet weak var profilePic: DesignableImageView!

    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var ageLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var followBtn: DesignableButton!
    
    @IBOutlet weak var mediaCollectionView : UICollectionView!
    @IBOutlet weak var mediaCollectionHgt : NSLayoutConstraint!
    @IBOutlet weak var postTbl : UITableView!
    @IBOutlet weak var postTblHgt : NSLayoutConstraint!
    
    var model           : ProfileModel!
    var createdPosts    = [PostModel]()
    var mediaArr        = [MediaModel]()

    var offset             = 0
    var isPageRefresing    = false
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillProfileDetails()
        self.mediaCollectionHgt.constant = 0.0
        self.postTblHgt.constant = 0.0
        getOtherUserAPI()
    }

    // MARK: -  Fill and Reload 
    func fillProfileDetails() {
        coverPicImg.downloadImage(url: model.coverPic_url, placeholder: "profile_bg")
        profilePic.downloadImage(url: model.profilePic_url, placeholder: "placeholder")
        
        profileNameLbl.text = model.display_name
        if (model.dob == nil || model.dob == "") {
            ageLbl.text = "NA"
            
        } else {
            let calendar = Calendar.current
            let date1 = model.dob?.getDatefor(format: DateFormats.birthDateFormat)
            let components = calendar.dateComponents([.year], from: date1!, to: Date())
            ageLbl.text = String(format: "%d years", components.year!)
        }
        addressLbl.text = (model.address == nil || model.address?.address == "") ? "NA" : (model.address?.address)!
        followBtn.isSelected = model.followed
    }
    
    func reloadCollectionView() {
        UIView.transition(with: self.mediaCollectionView, duration: 0.7, options: .transitionCrossDissolve, animations: {self.mediaCollectionView.reloadData()}, completion: nil)
        self.view.layoutIfNeeded()
        mediaCollectionHgt.constant = 175
        self.view.layoutIfNeeded()
    }

    func reloadTableView() {
        UIView.transition(with: self.postTbl, duration: 0.7, options: .transitionCrossDissolve, animations: {self.postTbl.reloadData()}, completion: nil)
        self.view.layoutIfNeeded()
        postTblHgt.constant = self.postTbl.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    // MARK: -  UIButton Action 
    @IBAction func followPersonAction(_ sender: UIButton) {
        if sender.isSelected {
            self.showAlert(alertMessage: "Are you sure, you want to unfollow?", buttonTitles: ["Cancel", "Unfollow"], style: .alert, action: { title in
                if title == "Unfollow" {
                    self.unFollowAPI(self.model.profile_id!)
                    sender.isSelected = !sender.isSelected
                }
            })
        } else {
            followAPI(model.profile_id!)
            sender.isSelected = !sender.isSelected
        }
    }
    
    @IBAction func initiateChatAction(_ sender: Any) {
        self.showAlert(alertMessage: ConstantModel.shared.message.comingSoon, buttonTitles: ["Ok"], style: .alert, action:nil)
    }
    
    @objc func likePressed(_ sender: UIButton) {
        let (feed, index)       = getPostFromCell(sender)
        
        likePostAPI(param: GetParamModel.shared.postLikeParam(postId: feed.postId, likePressed: sender.isSelected)!)
        sender.isSelected = !sender.isSelected
        feed.userLiked    = sender.isSelected
        
        let cell = postTbl.cellForRow(at: index) as! PostCell
        feed.totalLikes = feed.userLiked ? feed.totalLikes + 1 : feed.totalLikes - 1
        let likes = String(format: "%d %@", feed.totalLikes, (feed.totalLikes == 1 ? "like" : "likes"))
        cell.totalLikesBtn.setTitle(likes, for: .normal)
        cell.totalLikesBtn.isHidden = feed.userLiked ? false : (feed.totalLikes > 0 ? false : true)
    }
    
    @objc func joinPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            let (feed, _)       = getPostFromCell(sender)
            joinInterestAPI(param: GetParamModel.shared.joinInterestParam(postId: feed.postId, type: "join")!)
            sender.isSelected = !sender.isSelected
            feed.userJoined   = sender.isSelected
        }
    }

    @objc func interestedPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            let (feed, _)       = getPostFromCell(sender)
            joinInterestAPI(param: GetParamModel.shared.joinInterestParam(postId: feed.postId, type: "interest")!)
            sender.isSelected   = !sender.isSelected
            feed.userInterested = sender.isSelected
        }
    }
    
    func getPostFromCell(_ btn: UIButton) -> (PostModel, IndexPath) {
        let buttonPosition = btn.convert(CGPoint(x: 0, y: 0), to: postTbl)
        let indexPath      = postTbl.indexPathForRow(at: buttonPosition)
        return (createdPosts[indexPath!.row], indexPath!)
    }
    
    // MARK: -  API Call 
    func getOtherUserAPI() {
        let param = GetParamModel.shared.getOtherUserParam(model.profile_id!, offset)
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.getOtherUser, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in

            APIResponseModel.shared.parseGetProfileResponse(response: response, sender: self, callback: { result in
                
                let profileData  = result as? [String: Any]
                if self.offset == 0 {
                    self.createdPosts.removeAll()
                    self.model = profileData!["profile"] as? ProfileModel
                    self.fillProfileDetails()
                }
                
                self.createdPosts = profileData!["createdPosts"] as! [PostModel]
                if self.mediaArr.count == 0 {
                    self.mediaArr   = self.createdPosts.compactMap { $0.media }.flatMap {$0}
                    self.mediaArr.count > 0 ? self.reloadCollectionView() : (self.mediaCollectionHgt.constant = 0.0)
                }
                self.createdPosts.count > 0 ? self.reloadTableView() : (self.postTblHgt.constant = 0.0)
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
    
    func followAPI(_ userId: String) {
        let param = GetParamModel.shared.followUnFollowParam(userId)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.follow, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in
            })
            self.model.followed = true
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
    
    func unFollowAPI(_ userId: String) {
        let param = GetParamModel.shared.followUnFollowParam(userId)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.unFollow, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in
                self.model.followed = false
            })

        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
    
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOtherLikes" {
            let nextVC          = segue.destination as? LikesController
            let (post, _)       = getPostFromCell(sender as! UIButton)
            nextVC?.postId      = post.postId
            
        } else if segue.identifier == "showOtherComment" || segue.identifier == "postOtherComment" {
            let nextVC          = segue.destination as? CommentsController
            let (post, index)   = getPostFromCell(sender as! UIButton)
            nextVC?.postId      = post.postId
            
            nextVC?.updateCommentCount = { count in
                let model = self.createdPosts[index.row]
                model.totalComment = count
                
                let cell = self.postTbl.cellForRow(at: index) as! PostCell
                if model.totalComment > 0 {
                    cell.totalCommentsBtn.isHidden = false
                    let comments = String(format: "%d %@", model.totalComment, (model.totalComment == 1 ? "comment" : "comments"))
                    cell.totalCommentsBtn.setTitle(comments, for: .normal)
                }
            }
        }
    }
}

extension OthersProfileController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        isPageRefresing = false
        return createdPosts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherPostCellId") as! PostCell
        cell.joinBtn.addTarget(self, action: #selector(joinPressed(_:)), for: .touchUpInside)
        cell.interestedBtn.addTarget(self, action: #selector(interestedPressed(_:)), for: .touchUpInside)
        cell.likeBtn.addTarget(self, action: #selector(likePressed(_:)), for: .touchUpInside)
        
        cell.bindData(createdPosts[indexPath.row], myFeed: false)
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.postTbl.contentOffset.y >= (self.postTbl.contentSize.height - self.postTbl.bounds.size.height) {
            if isPageRefresing == false {
                // no need to worry about threads because this is always on main thread.
                isPageRefresing = true
                offset = createdPosts.count
                getOtherUserAPI()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.containerScroll {
            postTbl.isScrollEnabled = (self.containerScroll.contentOffset.y >= 200)
        }
        if scrollView == self.postTbl {
            self.postTbl.isScrollEnabled = (postTbl.contentOffset.y > 0)
        }
    }
}

// MARK: - collectionView Delegate and Datasource
extension OthersProfileController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let widthConstant = ((ConstantModel.shared.screenSize.width) / 3.0) - 2
        let size = CGSize(width: widthConstant, height: widthConstant)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserGalleryCollectionCellId", for: indexPath) as! UserGalleryCollectionCell
        let model = mediaArr[indexPath.row]
        cell.picImageView.downloadImage(url: model.uploadedUrl, placeholder: "placeholder")
        return cell
    }
}

