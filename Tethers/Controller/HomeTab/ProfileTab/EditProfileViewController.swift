//
//  EditProfileViewController.swift
//  Tethers
//
//  Created by signity on 25/09/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    @IBOutlet weak var cover_Pic: UIImageView!
    @IBOutlet weak var profile_Pic: DesignableImageView!

    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var dobTxt: UITextField!
    @IBOutlet weak var addressTxt: UITextField!
    
    var model             :  ProfileModel!
    var profileMedia      :  MediaModel?
    var coverMedia        :  MediaModel?

    var datePickerView    =  UIDatePicker()

    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        
        let listView       = EventManager.inputV(datePickerView, sender: self)
        dobTxt.tintColor   = .clear
        dobTxt.inputView   = listView
        datePickerView.datePickerMode = .date
        
        cover_Pic.sd_setShowActivityIndicatorView(true)
        cover_Pic.sd_setIndicatorStyle(.whiteLarge)
        
        profile_Pic.sd_setShowActivityIndicatorView(true)
        profile_Pic.sd_setIndicatorStyle(.whiteLarge)
        
        fillProfileDetails()
    }
    
    func fillProfileDetails() {
        cover_Pic.downloadImage(url: model.coverPic_url, placeholder: "profile_bg")
        profile_Pic.downloadImage(url: model.profilePic_url, placeholder: "placeholder")
        
        nameTxt.text       = model.display_name
        
        let calender        = Calendar.current
        let currentDate     = Date()
        var components      = DateComponents()
        
        components.year = -150
        let minDate = calender.date(byAdding: components, to: currentDate)
        datePickerView.minimumDate    = minDate
        
        components.year = -16
        let maxDate = calender.date(byAdding: components, to: currentDate)
        datePickerView.maximumDate    = maxDate
        dobTxt.text = model.dob ?? ""

        addressTxt.text = model!.address?.address ?? ""
    }
    
    // MARK: -  UIButton Action     
    @IBAction func changeCoverPicAction(_ sender: Any) {
        changePicture(isCover: true)
    }

    @IBAction func changeProfilePicAction(_ sender: Any) {
        changePicture(isCover: false)
    }
    
    func changePicture(isCover: Bool) {
        let url = isCover ? model.coverPic_url : model.profilePic_url
        self.openCameraGallerySheet(isRemove: EventManager.isValidUrl(url: url), action: {
            let pictureType = isCover ? "cover_picture" : "image_url"
            self.removePicAPI(picture_Type: pictureType, name: url!)
        })
        
        didfetchMedia = { picker, media in
            if isCover {
                self.coverMedia = media
                self.cover_Pic.image = EventManager.resizeImg(image: (self.coverMedia?.thumb)!, size: self.cover_Pic.frame.size)
                
            } else {
                self.profileMedia = media
                self.profile_Pic.image = EventManager.resizeImg(image: (self.profileMedia?.thumb)!, size: self.profile_Pic.frame.size)
            }
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func selectDateAction(_ sender: Any) {
        dobTxt.becomeFirstResponder()
    }
    
    @IBAction func locationAction(_ sender: Any) {
        self.openPlaces()
        locationCallback = { location in
            DispatchQueue.main.async {
                self.model.address   = location
                self.addressTxt.text = location.shortAddress
            }
        }
    }
    
    @IBAction func updateAction(_ sender: Any) {
        self.updateProfile()
    }
    
    func updateProfile() {
        EventManager.showloader()
        let queue = DispatchQueue(label: "com.company.app.queue", attributes: .concurrent)
        let group = DispatchGroup()
        
        if coverMedia != nil {
            group.enter()
            queue.async(group: group) {
                AWSUploader.uploadMediaToAWS(self.coverMedia!, uploadFinish: { (status, url) in
                    self.coverMedia?.uploadedUrl = url
                    group.leave()
                })
            }
        }
        
        if profileMedia != nil {
            group.enter()
            queue.async(group: group) {
                AWSUploader.uploadMediaToAWS(self.profileMedia!, uploadFinish: { (status, url) in
                    self.profileMedia?.uploadedUrl = url
                    group.leave()
                })
            }
        }
        
        group.notify(queue: queue) {
            DispatchQueue.main.async {
                EventManager.hideloader()
                self.model.display_name = self.nameTxt.text!
                self.updateProfileAPI()
            }
        }
    }
    
    @objc func resignTextField(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        if sender.tag == 101 {
            //For date format
            let dateStr = datePickerView.date.getDatefor(format: DateFormats.birthDateFormat)
            dobTxt.text = dateStr
            model.dob   = dateStr
        }
    }
    
    // MARK: -  API Call 
    func updateProfileAPI() {
        let param = GetParamModel.shared.getUpdateUserParam(model: model)!
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.editUser, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseUpdateProfileResponse(model: self.model, sender: self, callback: { result in
                
                self.showAlert(alertMessage: ConstantModel.shared.message.profileUpdate, buttonTitles: ["Ok"], style: .alert, action: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    func removePicAPI(picture_Type: String, name: String) {
        let param = GetParamModel.shared.getRemovePicParam(type: picture_Type, name: name)!
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.removePhoto, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                let user = UserDefaultsHandler.userInfo
                if picture_Type == "cover_picture" {
                    self.coverMedia      = nil
                    self.model.coverPic_url  = nil

                    user?.coverPic_url       = nil
                    self.cover_Pic.image     = UIImage.init(named: "profile_bg")
                    
                } else {
                    self.profileMedia      = nil
                    self.model.profilePic_url  = nil
                    
                    user?.profilePic_url       = nil
                    self.profile_Pic.image     = UIImage.init(named: "placeholder")
                }
                UserDefaultsHandler.userInfo = user
                self.showAlert(alertMessage: response!["message"] as? String ?? "", buttonTitles: ["Ok"], style: .alert, action: nil)
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}
