//
//  ChangePasswordController.swift
//  Tethers
//
//  Created by Pooja Rana on 23/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ChangePasswordController: UIViewController {

    @IBOutlet weak var oldPasswordTxt: DesignableTextField!
    @IBOutlet weak var newPasswordTxt: DesignableTextField!
    @IBOutlet weak var confirmPasswordTxt: DesignableTextField!
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: -  UIButton Action 
    @IBAction func changePasswordAction(_ sender: Any) {
        if self.validateChangePasswordUp() {
            changePasswordAPI()
        }
    }
    
    // MARK: -  Validation 
    func validateChangePasswordUp() -> Bool {
        
        self.view.endEditing(true)
        if oldPasswordTxt.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if newPasswordTxt.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterNewPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if newPasswordTxt.text!.isNotValidPassword() {
            self.showAlert(alertMessage: ConstantModel.shared.message.validPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if confirmPasswordTxt.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.confirmPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if newPasswordTxt.text != confirmPasswordTxt.text {
            self.showAlert(alertMessage: ConstantModel.shared.message.matchPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else {
            return true
        }
    }
    
    // MARK: -  API Call 
    func changePasswordAPI() {
        
        let param = GetParamModel.shared.getChangePassParam(oldPassword: oldPasswordTxt.text!, newPassword: newPasswordTxt.text!)!
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.changePassword, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                
                self.showAlert(alertMessage: ConstantModel.shared.message.passwordUpdate, buttonTitles: ["Ok"], style: .alert, action: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}
