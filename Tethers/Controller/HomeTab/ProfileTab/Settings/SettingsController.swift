//
//  SettingsController.swift
//  Tethers
//
//  Created by Pooja Rana on 23/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {

    @IBOutlet weak var settingsTbl: UITableView!
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        settingsTbl.tableFooterView = UIView.init(frame: .zero)
    }

    // MARK: -  UIButton Action 
    @IBAction func backBtnAction(_ sender: Any) {
        customPushPop(nil, ispush: false)
    }
}

extension SettingsController: UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //Don't show change password to social logged in user
//        return indexPath.row == 3 ? 120 : (UserDefaultsHandler.userInfo?.account_type == "social" && indexPath.row == 2) ? 0.0 : 60.0
        return indexPath.row == 3 ? 120 : 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: String(format: "SettingsCellId%d", indexPath.row))!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 2:
            self.performSegue(withIdentifier: "showChangePassword", sender: nil)
        case 3:
            break
        default:
            showAlert(alertMessage: ConstantModel.shared.message.comingSoon, buttonTitles: ["OK"], style: .alert, action: nil)
        }
    }
}
