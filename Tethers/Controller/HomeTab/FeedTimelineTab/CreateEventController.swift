//
//  CreateEventController.swift
//  Tethers
//
//  Created by Pooja Rana on 27/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class CreateEventController: UIViewController {

    @IBOutlet weak var eventPicView: UIImageView!
    @IBOutlet weak var eventTitleTxtFld: UITextField!
    @IBOutlet weak var eventDescription: UITextView!
    @IBOutlet weak var eventLocTxtFld: UITextField!
    @IBOutlet weak var eventDateLbl: UITextField!
    @IBOutlet weak var eventTimeLbl: UITextField!

    var datePickerView    =  UIDatePicker()
    var timePickerView    =  UIDatePicker()
    var postModel = PostModel()

    var updateEvent: ((PostModel) -> Void)?
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        datePickerView.minimumDate    = Date()
        timePickerView.minimumDate    = Date()

        eventDateLbl.tintColor   = .clear
        eventDateLbl.inputView   = EventManager.inputV(datePickerView, sender: self)
        datePickerView.datePickerMode = .date
        
        eventTimeLbl.tintColor   = .clear
        eventTimeLbl.inputView   = EventManager.inputV(timePickerView, sender: self)
        timePickerView.datePickerMode = .time
        
        postModel.media = [MediaModel]()
        UpdateLocationsModal.shared.determineMyCurrentLocation()
    }

    // MARK: -  UIButton Action 
    @IBAction func editEventPicAction(_ sender: Any) {
        self.openCameraGallerySheet(isRemove: false, action: nil)
        didfetchMedia = { picker, model in
            picker.dismiss(animated: false) {
                self.postModel.media?.removeAll()
                self.postModel.media?.append(model)
                self.eventPicView.image = model.thumb
            }
        }
    }
    
    @IBAction func getEventLocAction(_ sender: Any) {
        self.openPlaces()
        locationCallback = { address in
            
            DispatchQueue.main.async {
                self.postModel.taggedLocation = address
                self.eventLocTxtFld.text = self.postModel.taggedLocation!.address
            }
        }
    }
    
    @objc func resignTextField(_ sender: UIBarButtonItem) {
        if sender.tag == 101 {
            let dateStr = eventDateLbl.isFirstResponder ? datePickerView.date.getDatefor(format: DateFormats.eventDateFormat) : timePickerView.date.getDatefor(format: DateFormats.eventTimeFormat)
            _ = eventDateLbl.isFirstResponder ? (postModel.eventDate = dateStr, eventDateLbl.text = dateStr) : (postModel.eventTime = dateStr, eventTimeLbl.text = dateStr)
        }
        self.view.endEditing(true)
    }
    
    @IBAction func createEventAction(_ sender: Any) {
        postModel.postTitle = eventTitleTxtFld.text!
        postModel.postDescription = eventDescription.text!
        
        if validateEvent() {
            if ConstantModel.shared.currentAddress.lat != "" && ConstantModel.shared.currentAddress.lng != "" {
                self.postModel.current_lat = ConstantModel.shared.currentAddress.lat
                self.postModel.current_long = ConstantModel.shared.currentAddress.lng
                self.createEvent()
                
            } else {
                self.showAlert(alertMessage: ConstantModel.shared.message.locationNetError, buttonTitles: ["Ok"], style: .alert, action:{ _ in
                    UpdateLocationsModal.shared.determineMyCurrentLocation()
                })
            }
        }
    }
    
    func createEvent() {
        postModel.tags = [TagModel]()
        postModel.postType          = "event"
        postModel.ageCategory       = "all"
        postModel.canJoin           = false
        postModel.allowCommentLike  = true
        
        postModel.postCreator = UserDefaultsHandler.userInfo
        
        let media = postModel.media![0]
        if media.uploadedUrl == "" {
            EventManager.showloader()
            AWSUploader.uploadMediaToAWS(media, uploadFinish: { (status, url) in
                if status { media.uploadedUrl = url }
                EventManager.hideloader()
                self.createEventAPI()
            })
        } else {
            self.createEventAPI()
        }
    }
    
    // MARK: -  UIButton Action 
    func validateEvent() -> Bool {
        if postModel.media?.count == 0 {
            self.showAlert(alertMessage: ConstantModel.shared.message.addEventImage, buttonTitles: ["OK"], style: .alert, action: nil)
            return false

        } else if postModel.postTitle.getTrimmedStr().isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.addEventTitle, buttonTitles: ["OK"], style: .alert, action: nil)
            return false

        } else if postModel.postDescription.getTrimmedStr().isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.addEventDescription, buttonTitles: ["OK"], style: .alert, action: nil)
            return false

        } else if postModel.taggedLocation == nil {
            self.showAlert(alertMessage: ConstantModel.shared.message.addEventLocation, buttonTitles: ["OK"], style: .alert, action: nil)
            return false

        } else if postModel.eventDate.isStringEmpty() || postModel.eventTime.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.addEventDateTime, buttonTitles: ["OK"], style: .alert, action: nil)
            return false

        } else {
            return true
        }
    }
    
    // MARK: -  API Call 
    func createEventAPI() {
        let param = GetParamModel.shared.getUploadPostParam(model: self.postModel)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.uploadPost, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                self.updateEvent?(self.postModel)
                self.showAlert(alertMessage: response!["message"] as? String ?? "", buttonTitles: ["Ok"], style: .alert, action: { _ in
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}
