//
//  CommentsController.swift
//  Tethers
//
//  Created by Pooja Rana on 27/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class CommentsController: UIViewController {
    
    @IBOutlet weak var commentsTbl: UITableView!
    @IBOutlet weak var picImgView: UIImageView!
    @IBOutlet weak var commentTxtFld: UITextField!

    var postId: String!
    var myComments = [LikeCommentModel]()
    var updateCommentCount: ((Int) -> Void)?
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        commentsTbl.tableFooterView = UIView.init(frame: .zero)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        picImgView.downloadImage(url: UserDefaultsHandler.userInfo?.profilePic_url, placeholder: "placeholder")
        getCommentsAPI()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: -  UIButton Action 
    @IBAction func sendCommentAction(_ sender: Any) {
        if commentTxtFld.text!.getTrimmedStr().isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterComment, buttonTitles: ["Ok"], style: .alert, action:nil)

        } else {
            self.view.endEditing(true)
            postCommentAPI()
        }
    }
    
    // MARK: -  Keyboard Notification 
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            var contentInset:UIEdgeInsets = self.commentsTbl.contentInset
            contentInset.bottom = keyboardSize.height
            commentsTbl.contentInset = contentInset
            scrolltoBottom(animate: false)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        commentsTbl.contentInset = UIEdgeInsets.zero
    }
    
    func scrolltoBottom(animate: Bool) {
        let lastIndexPath = IndexPath(row: self.myComments.count - 1, section: 0)
        self.commentsTbl.scrollToRow(at: lastIndexPath,
                                     at: UITableView.ScrollPosition.bottom,
                                     animated: animate)
    }
    
    // MARK: -  API Call 
    func getCommentsAPI() {
        let param = GetParamModel.shared.getLikesCommentsParam(postId: postId)
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.getComments, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseLikesComments(response: response, sender: self, callback: { result in
                self.myComments = result as! [LikeCommentModel]
                self.commentsTbl.reloadData()
                
                if self.myComments.count > 0 {
                    self.scrolltoBottom(animate: false)
                }
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    func postCommentAPI() {
        let param = GetParamModel.shared.postCommentParam(postId: postId, content: commentTxtFld.text!)
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.commentPost, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in
                let model     = LikeCommentModel()
                model.comment = self.commentTxtFld.text!
                model.user = UserDefaultsHandler.userInfo!
                
                self.myComments.append(model)
                self.updateCommentCount?(self.myComments.count)
                self.commentTxtFld.text = ""
                
                self.updateLastRow()
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    func updateLastRow() {
        DispatchQueue.main.async {
            let lastIndexPath = IndexPath(row: self.myComments.count - 1, section: 0)
            
            self.commentsTbl.beginUpdates()
            self.commentsTbl.insertRows(at: [lastIndexPath], with: .none)
            self.commentsTbl.endUpdates()
            
            self.commentsTbl.layoutIfNeeded()
            self.scrolltoBottom(animate: false)
        }
    }
}

extension CommentsController: UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myComments.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCellId") as! CommentCell
        cell.bindData(myComments[indexPath.row])
        return cell
    }
}
