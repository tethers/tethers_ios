//
//  LikesController.swift
//  Tethers
//
//  Created by Pooja Rana on 27/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class LikesController: UIViewController {

    @IBOutlet weak var likesTbl: UITableView!
    var postId: String!
    var myLikes = [LikeCommentModel]()

    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        likesTbl.tableFooterView = UIView.init(frame: .zero)
        getLikesAPI()
    }
    
    // MARK: -  API Call 
    func getLikesAPI() {
        let param = GetParamModel.shared.getLikesCommentsParam(postId: postId)
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.getLikes, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseLikesComments(response: response, sender: self, callback: { result in
                self.myLikes = result as! [LikeCommentModel]
                self.likesTbl.reloadData()
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}

extension LikesController: UITableViewDelegate, UITableViewDataSource  {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myLikes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikeCellId") as! LikeCell
        cell.bindData(myLikes[indexPath.row])
        return cell
    }
}
