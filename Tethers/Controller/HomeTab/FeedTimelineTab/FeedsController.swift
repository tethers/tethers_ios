//
//  FeedsController.swift
//  Tethers
//
//  Created by Pooja Rana on 10/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class FeedsController: UIViewController {

    // MARK:- Outlets.
    @IBOutlet weak var feedsTbl: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK:- Refrences.
    var nearbyEvents     = [PostModel]()
    var myFeeds          = [PostModel]()
    let refreshControl     = UIRefreshControl()
    var offset             = 0
    var isPageRefresing    = false
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        feedsTbl.estimatedRowHeight = 497.0
        
        refreshControl.addTarget(self, action: #selector(refreshTab), for: .valueChanged)
        feedsTbl.refreshControl = refreshControl
        searchBar.changeSearchbarColor()
        refreshTab()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc func refreshTab() {
        offset = 0
        getPostsAPI(isSilent: false)
        getNearByEventsAPI()
    }
    
    @IBAction func filterBtnAction(_ sender: Any) {
        let filterSearchVC = storyboard?.instantiateViewController(withIdentifier: "FilterSearchVC") as! FilterSearchVC
        filterSearchVC.comeFrom       = .myFeed
        navigationController?.pushViewController(filterSearchVC, animated: true)
    }
    
    @objc func likePressed(_ sender: UIButton) {
        let (feed, index)       = getPostFromCell(sender)

        likePostAPI(param: GetParamModel.shared.postLikeParam(postId: feed.postId, likePressed: sender.isSelected)!)
        sender.isSelected = !sender.isSelected
        feed.userLiked    = sender.isSelected
        
        let cell = feedsTbl.cellForRow(at: index) as! PostCell
        feed.totalLikes = feed.userLiked ? feed.totalLikes + 1 : feed.totalLikes - 1
        let likes = String(format: "%d %@", feed.totalLikes, (feed.totalLikes == 1 ? "like" : "likes"))
        cell.totalLikesBtn.setTitle(likes, for: .normal)
        cell.totalLikesBtn.isHidden = feed.userLiked ? false : (feed.totalLikes > 0 ? false : true)
    }
    
    @objc func joinPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            let (feed, _)       = getPostFromCell(sender)
            joinInterestAPI(param: GetParamModel.shared.joinInterestParam(postId: feed.postId, type: "join")!)
            sender.isSelected = !sender.isSelected
            feed.userJoined   = sender.isSelected
        }
    }
    
    @objc func interestedPressed(_ sender: UIButton) {
        if sender.isSelected == false {
            let (feed, _)       = getPostFromCell(sender)
            joinInterestAPI(param: GetParamModel.shared.joinInterestParam(postId: feed.postId, type: "interest")!)
            sender.isSelected   = !sender.isSelected
            feed.userInterested = sender.isSelected
        }
    }
    
    // MARK: -  API Call 
    func getPostsAPI(isSilent: Bool) {
        let param = GetParamModel.shared.getListWithPagination(offset: offset)
        ApiCallModel.shared.apiCall(isSilent: isSilent, url: ConstantModel.shared.api.getPost, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseGetPostsResponse(response: response, sender: self, callback: { result in
                if self.offset == 0 { self.myFeeds.removeAll() }
                let moreFeeds = result as! [PostModel]
                if moreFeeds.count > 0 {
                    self.myFeeds.append(contentsOf: moreFeeds)
                    self.feedsTbl.reloadData()
                }
                self.refreshControl.endRefreshing()

            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
            self.refreshControl.endRefreshing()
        })
    }
    
    func getNearByEventsAPI() {
        let param = GetParamModel.shared.getNearestEvents()
        ApiCallModel.shared.apiCall(isSilent: true, url: ConstantModel.shared.api.getNearestEvent, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseGetPostsResponse(response: response, sender: self, callback: { result in
                self.nearbyEvents.append(contentsOf: result as! [PostModel])
                self.feedsTbl.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
        
    // MARK: -  Navigation 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLikes" {
            let nextVC          = segue.destination as? LikesController
            let (post, _)       = getPostFromCell(sender as! UIButton)
            nextVC?.postId      = post.postId

        } else if segue.identifier == "showComment" || segue.identifier == "postComment" {
            let nextVC          = segue.destination as? CommentsController
            let (post, index)   = getPostFromCell(sender as! UIButton)
            nextVC?.postId      = post.postId
            
            nextVC?.updateCommentCount = { count in
                let model = self.myFeeds[index.row-1]
                model.totalComment = count
                
                let cell = self.feedsTbl.cellForRow(at: index) as! PostCell
                if model.totalComment > 0 {
                    cell.totalCommentsBtn.isHidden = false
                    let comments = String(format: "%d %@", model.totalComment, (model.totalComment == 1 ? "comment" : "comments"))
                    cell.totalCommentsBtn.setTitle(comments, for: .normal)
                }
            }
        } else if segue.identifier == "createEvent" {
            let nextVC          = segue.destination as? CreateEventController
            
            nextVC?.updateEvent = { feed in
                self.myFeeds.insert(feed, at: 0)

                DispatchQueue.main.async {
                    self.feedsTbl.beginUpdates()
                    self.feedsTbl.insertRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                    self.feedsTbl.endUpdates()
                    self.feedsTbl.layoutIfNeeded()
                }
            }
        }
    }
    
    func getPostFromCell(_ btn: UIButton) -> (PostModel, IndexPath) {
        let buttonPosition = btn.convert(CGPoint(x: 0, y: 0), to: feedsTbl)
        let indexPath      = feedsTbl.indexPathForRow(at: buttonPosition)
        
        return (myFeeds[indexPath!.row-1], indexPath!)
    }
}

extension FeedsController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: -  TableView Delegates & Datasource 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.isPageRefresing = false
        return myFeeds.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? (nearbyEvents.count > 0 ? 128 : 0) : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            //if first cell and nearby events available then show collection
            let cell = tableView.dequeueReusableCell(withIdentifier: "NearestEventCellId") as! NearestEventCell
            return cell

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellId") as! PostCell
            cell.joinBtn.addTarget(self, action: #selector(joinPressed(_:)), for: .touchUpInside)
            cell.interestedBtn.addTarget(self, action: #selector(interestedPressed(_:)), for: .touchUpInside)
            cell.likeBtn.addTarget(self, action: #selector(likePressed(_:)), for: .touchUpInside)
            
            cell.bindData(myFeeds[indexPath.row - 1], myFeed: true)
            return cell
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.feedsTbl.contentOffset.y >= (self.feedsTbl.contentSize.height - self.feedsTbl.bounds.size.height) {
            if isPageRefresing == false {
                // no need to worry about threads because this is always on main thread.
                isPageRefresing = true
                offset = myFeeds.count
                getPostsAPI(isSilent: true)
            }
        }
    }
}

extension FeedsController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        //show places from google place api.
        self.openPlaces()
        locationCallback = { address in
            searchBar.text = address.address
        }
        self.view.crossDissolveAnimation(duration: 0.2)
        return false
        
    }
}
