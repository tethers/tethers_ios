//
//  ChatListController.swift
//  Tethers
//
//  Created by Pooja Rana on 10/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ChatListController: UIViewController{

    //MARK: -  Outlets
    @IBOutlet weak var chatsTableView: UITableView!
    
    //MARK: -  Refrences
    let currentUser        = UserDefaultsHandler.userInfo
    var messagesDictionary = [String: Message]()
    var timer              : Timer?
    var messages           = [Message]()
    let cellId             = "cellId"
    var usersList          = [ProfileModel]()
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        let image = UIImage(named: "new_message_icon")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleNewMessage))
        self.title = "All Chats"
        
        chatsTableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        chatsTableView.delegate = self
        chatsTableView.dataSource = self
        
        //Get the following users list.
        getFollowings()
        
        //Get user's chats.
        observeUserMessages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.prefersLargeTitles = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //Move to All users screen.
    @objc func handleNewMessage() {
        let followerFollowingList = storyboard?.instantiateViewController(withIdentifier: "FollowerFollowingList") as! FollowerFollowingList
        followerFollowingList.isComeFromChat = true
        followerFollowingList.usersList = usersList
        followerFollowingList.isFollowers = false
        navigationController?.pushViewController(followerFollowingList, animated: true)
    }
    
    // MARK: -  API Call 
    func getFollowings() {
        let url = ConstantModel.shared.api.getUserFollowing
        let param = GetParamModel.shared.getListWithPagination(offset: 0)
        
        ApiCallModel.shared.apiCall(isSilent: false, url:url, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseFollowerFollowings(response: response, isFollowers: false, sender: self, callback: { result in
                let moreProfiles = result as! [ProfileModel]
                if moreProfiles.count > 0 {
                    self.usersList.append(contentsOf: moreProfiles)
                }
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
    
    
    //Observe user's chats.
    func observeUserMessages() {
        guard let uid = currentUser?.profile_id else { return }
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        ref.observe(.childAdded, with: { (snapshot) in
            
            let userId = snapshot.key
            ref.child(userId).observe(.childAdded, with: { (snapshot) in
                
                let messageId = snapshot.key
                self.fetchMessageWithMessageId(messageId)
                
            }, withCancel: nil)
            
        }, withCancel: nil)
    }
    
    //Fetch message by particular message id of particulat chat.
    fileprivate func fetchMessageWithMessageId(_ messageId: String) {
        let messagesReference = Database.database().reference().child("messages").child(messageId)
        
        messagesReference.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let message = Message(dictionary: dictionary)
                if let chatPartnerId = message.chatPartnerId() {
                    self.messagesDictionary[chatPartnerId] = message
                }
                self.attemptReloadOfTable()
            }
        }, withCancel: nil)
    }
    
    fileprivate func attemptReloadOfTable() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    @objc func handleReloadTable() {
        self.messages = Array(self.messagesDictionary.values)
        self.messages.sort(by: { (message1, message2) -> Bool in
            return message1.timestamp!.int32Value > message2.timestamp!.int32Value
        })
        
        //this will crash because of background thread, so lets call this on dispatch_async main thread
        DispatchQueue.main.async(execute: {
            self.chatsTableView.reloadData()
        })
    }
    
    func showChatControllerForUser(_ user: ProfileModel) {
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.user = user
        navigationController?.pushViewController(chatLogController, animated: true)
    }
}

extension ChatListController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        let message    = messages[indexPath.row]
        cell.userModel = usersList
        cell.message   = message
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]

        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }

        //get the user of this particular id.
        let user = usersList.filter { (model) -> Bool in
            if model.profile_id == chatPartnerId{
                return true
            }
            return false
        }
        self.showChatControllerForUser(user[0])
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        guard let uid = currentUser?.profile_id else {
            return
        }
        
        let message = self.messages[indexPath.row]
        
        if let chatPartnerId = message.chatPartnerId() {
            Database.database().reference().child("user-messages").child(uid).child(chatPartnerId).removeValue(completionBlock: { (error, ref) in
                
                if error != nil {
                    print("Failed to delete message:", error!)
                    return
                }
                
                self.messagesDictionary.removeValue(forKey: chatPartnerId)
                self.attemptReloadOfTable()
            })
        }
    }
}
