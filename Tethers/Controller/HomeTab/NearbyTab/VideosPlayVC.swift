//
//  VideosPlayVC.swift
//  Tethers
//
//  Created by signity on 18/02/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit
import AVKit

class VideosPlayVC: UIViewController {

    @IBOutlet weak var videosTableView: UITableView!
    var media = MediaModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addViewCount()
    }
   
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func addViewCount(){
        let params = ["post_id":media.postId,"media_id":media.mediaId]
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.addMediaCount, parameters: params, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            print(response)
            APIResponseModel.shared.parseGetInterestResponse(response: response, sender: self, callback: { result in

            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}

extension VideosPlayVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as? VideoTableViewCell else {
            return VideoTableViewCell()
        }
        let url = media.uploadedUrl
        let avPlayer = AVPlayer(url: URL(string: url)!)
        cell.playerView.playerLayer.player = avPlayer
        cell.playerView.contentMode = .scaleAspectFill
        cell.playerView.backgroundColor = .black
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = (cell as? VideoTableViewCell) else { return };
        videoCell.playerView.player?.play()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let videoCell = cell as? VideoTableViewCell else { return }
        videoCell.playerView.player?.pause()
        videoCell.playerView.player = nil
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height
    }
    
    
}

class VideoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var backButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
