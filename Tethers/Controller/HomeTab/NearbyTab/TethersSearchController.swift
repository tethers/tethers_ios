//
//  TethersSearchController.swift
//  Tethers
//
//  Created by Pooja Rana on 03/01/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit

class TethersSearchController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchedTblView: UITableView!

    var searchUserArray         = [ProfileModel]()
    var shouldBeginEditing      = true
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchedTblView.tableFooterView = UIView.init(frame: .zero)
        self.tabBarController?.tabBar.isHidden = true
        
        searchBar.becomeFirstResponder()
    }
    
    // MARK: -  UIButton Action 
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.view.crossDissolveAnimation(duration: 0.2)
    }
    
    // MARK: -  API Call 
    func searchAPI(searchKeyword: String) {
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.searchUser + searchKeyword, parameters: nil, type: ConstantModel.shared.api.GetAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseGetProfiles(response: response, sender: self, callback: { result in
                self.searchUserArray = result as! [ProfileModel]
                self.searchedTblView.reloadData()
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
            self.clearSearch()
        })
    }
    
    func clearSearch() {
        searchUserArray.removeAll()
        searchedTblView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOtherUser" {
            let nextVC          = segue.destination as? OthersProfileController
            nextVC?.model       = sender as? ProfileModel
        }
    }
}

extension TethersSearchController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: -  UITableView Delegates 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchUserArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell                 = tableView.dequeueReusableCell(withIdentifier: String(format: "LikeCellId", indexPath.row)) as! LikeCell
        cell.bindData(searchUserArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model               = searchUserArray[indexPath.row]
        model.profile_id == UserDefaultsHandler.userInfo?.profile_id ? (self.tabBarController?.selectedIndex = 1) : self.performSegue(withIdentifier: "showOtherUser", sender: model)
    }
    
    // MARK: -  UISearchBar Delegates 
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText.count == 0) {
            shouldBeginEditing = false
            self.view.endEditing(true)
            self.clearSearch()
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        let boolToReturn = shouldBeginEditing
        shouldBeginEditing = true
        return boolToReturn
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        shouldBeginEditing = true
        self.searchAPI(searchKeyword: searchBar.text!)
        self.view.endEditing(true)
    }
}
