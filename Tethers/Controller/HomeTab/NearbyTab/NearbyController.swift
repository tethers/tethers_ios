//
//  NearbyController.swift
//  Tethers
//
//  Created by Pooja Rana on 10/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import GoogleMaps
import collection_view_layouts
import AVKit

var filterResponse : ((_ maxDistance: Int?, _ ageRange: String?) -> Void)?
class NearbyController: UIViewController{
    
    // MARK:- Outlets.
    @IBOutlet weak var mapContainerView: GMSMapView!
    @IBOutlet weak var interestCollectionView: UICollectionView!
    @IBOutlet weak var photosStackView: UIStackView!
    @IBOutlet weak var feedTableView: UITableView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var tableViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var photoStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK:- Refrences.
    private var clusterManager  : GMUClusterManager!
    var markerArr               = [POIItem]()
    private var cellsSizes      = [CGSize]()
    var offset                  = 0
    var detailedView            = DetailedView()
    var nearbyEvents            = [PostModel]()
    var interestArray           = [InterestModel]()
    var mediaArray              = [MediaModel]()
    var timer                   : Timer?
    let serviceGroup            = DispatchGroup()
    var updatedAddress          = AddressModel()
    var selectedInterestStrings = [String]()
    var maximumDistance         : Int!
    var ageCategory             : String!
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        mapContainerView.mapType = .normal
        searchBar.changeSearchbarColor()
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapContainerView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapContainerView, algorithm: algorithm,
                                           renderer: renderer)

        clusterManager.cluster()

        // Register self to listen to both GMUClusterManagerDelegate and
        // GMSMapViewDelegate events.
        clusterManager.setDelegate(self, mapDelegate: self)
        
        setupFlowLayout()
        
        //Update user's current location.
        UpdateLocationsModal.shared.determineMyCurrentLocation()
        
        getFilterResponse()
        
        getInterests()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //Setup flow layout of Photo grid.
    fileprivate func setupFlowLayout() {
        //set Photos collectionView layout.
        let contentFlowLayout: ContentDynamicLayout = InstagramStyleFlowLayout()
        contentFlowLayout.delegate = self
        contentFlowLayout.contentPadding = ItemsPadding(horizontal: 10, vertical: 10)
        contentFlowLayout.cellsPadding = ItemsPadding(horizontal: 8, vertical: 8)
        contentFlowLayout.contentAlign = .left
        photosCollectionView.collectionViewLayout = contentFlowLayout
    }
    
    //callback according to filter screen.
    fileprivate func getFilterResponse() {
        filterResponse = { (maxDistance, ageRange) in
            if maxDistance != nil || ageRange != nil{
                self.maximumDistance = maxDistance
                self.ageCategory     = ageRange
                //hit search api with these params.
                self.searchAPI(lat: self.updatedAddress.lat, long: self.updatedAddress.lng, interests: self.selectedInterestStrings, maxDistance: self.maximumDistance, ageRange: self.ageCategory)
            }
        }
    }
    
    // MARK: -  Get the user's selected interests.
    func getInterests(){
        ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.getInterest, parameters: nil, type: ConstantModel.shared.api.GetAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseGetInterestResponse(response: response, sender: self, callback: { result in
                self.interestArray.removeAll()
                self.selectedInterestStrings.removeAll()
                self.interestArray = result as! [InterestModel]
                self.selectedInterestStrings = self.interestArray.filter({$0.isSelected == true}).map({$0.name})
                self.interestCollectionView.reloadData()
                
                self.getNearbyEvents()
            })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    //Get nearby events and post based on location.
    func getNearbyEvents(){
        if ConstantModel.shared.currentAddress.lat != "" && ConstantModel.shared.currentAddress.lng != "" {
            updatedAddress = ConstantModel.shared.currentAddress
            searchAPI(lat: ConstantModel.shared.currentAddress.lat, long: ConstantModel.shared.currentAddress.lng, interests: selectedInterestStrings, maxDistance: self.maximumDistance, ageRange: self.ageCategory)
        } else {
            self.showAlert(alertMessage: ConstantModel.shared.message.locationNetError, buttonTitles: ["Ok"], style: .alert, action:{ _ in
                UpdateLocationsModal.shared.determineMyCurrentLocation()
                self.getNearbyEvents()
            })
        }
    }
    
   
    // MARK: -  InterestBtn tapped.
    @IBAction func interestBtnAction(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: interestCollectionView)
        let indexPath      = interestCollectionView.indexPathForItem(at: buttonPosition)
        let cell           = interestCollectionView.cellForItem(at: indexPath!) as? customCollectionCell
        
        if selectedInterestStrings.contains((cell?.interestBtn.titleLabel?.text)!){
            //remove.
            let index = selectedInterestStrings.firstIndex(of: (cell?.interestBtn.titleLabel?.text)!)
            selectedInterestStrings.remove(at: index!)
            let _ = interestArray.filter({$0.name == (cell?.interestBtn.titleLabel?.text)!}).map({ $0.isSelected = false })
        }else{
            selectedInterestStrings.append((cell?.interestBtn.titleLabel?.text)!)
            let _ = interestArray.filter({$0.name == (cell?.interestBtn.titleLabel?.text)!}).map({ $0.isSelected = true })
        }
    
        interestCollectionView.reloadItems(at: [indexPath!])
        self.searchAPI(lat: updatedAddress.lat, long: updatedAddress.lng, interests: selectedInterestStrings, maxDistance: self.maximumDistance, ageRange: self.ageCategory)
        
    }
    
    // MARK: -  Open search setting screen.
    @IBAction func openFilterAction(_ sender: Any) {
        let filterSearchVC = storyboard?.instantiateViewController(withIdentifier: "FilterSearchVC") as! FilterSearchVC
        filterSearchVC.comeFrom       = .discover
        filterSearchVC.sliderProgress = maximumDistance != nil ? maximumDistance : 100
        filterSearchVC.ageCatagory    = ageCategory != nil ? ageCategory : "all"
        navigationController?.pushViewController(filterSearchVC, animated: true)
    }
    
    
    // MARK: -  API Call 
    func searchAPI(lat:String, long:String, interests:[String], maxDistance:Int?, ageRange:String?) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: interests, options: .prettyPrinted)
            let selectedInterests = String(data: jsonData, encoding: .utf8)
            
            let params = ["lat": lat,
                          "long": long,
                          "maxdistance": maxDistance ?? 0,
                          "agecatagory": ageRange ?? "all",
                          "interests":selectedInterests!] as [String : Any]
            ApiCallModel.shared.apiCall(isSilent: true, url:ConstantModel.shared.api.getNearestEvent, parameters: params, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
                APIResponseModel.shared.parseGetPostsResponse(response: response, sender: self, callback: { result in
                    self.nearbyEvents.removeAll()
                    self.mediaArray.removeAll()
                    self.nearbyEvents.append(contentsOf: result as! [PostModel])
                    self.getMarkersArray(nearbyEventsArr: self.nearbyEvents)
                    self.mediaArray = self.nearbyEvents.compactMap { $0.media }.flatMap {$0}
                    self.feedTableView.reloadData()
                    self.photosCollectionView.reloadData()
                    self.resizeViews()
                })
            }, failure: { failureResponse in
                self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
            })
        }  catch {
            print(error.localizedDescription)
        }
    }
    
    //make markerArr from PostModel.
    func getMarkersArray(nearbyEventsArr:[PostModel]){
        self.markerArr.removeAll()
        //Make the marker array.
        for event in nearbyEventsArr{
            let title    = event.postTitle
            if event.taggedLocation == nil{
                if event.current_lat != ""{
                    let item     = POIItem(position: CLLocationCoordinate2DMake(Double(event.current_lat)!, Double(event.current_long)!), name: title, image: event.media != nil ? event.media![0].uploadedUrl : "placeholder")
                    self.markerArr.append(item)
                }
            }else{
                
                //first check if it is image or video.
               // if event.media![0].isImage{
                    let item     = POIItem(position: CLLocationCoordinate2DMake(Double(event.taggedLocation!.lat)!, Double(event.taggedLocation!.lng)!), name: title, image: event.media != nil ? event.media![0].uploadedUrl : "placeholder")
                    self.markerArr.append(item)
//                }else{
//                    //it is video.
//                    //generate thumbnale for initializing the marker array item.
//                    let thumbnail = self.createThumbnailOfVideoFromRemoteUrl(url: event.media![0].uploadedUrl)
//                    let item     = POIItem(position: CLLocationCoordinate2DMake(Double(event.taggedLocation!.lat)!, Double(event.taggedLocation!.lng)!), name: title, thumb: thumbnail!)
//                    self.markerArr.append(item)
//                }
            }
        }
        
        if markerArr.count > 0{
            clusterManager.add(markerArr)
            setCameraOnMap()
        }else{
            clusterManager.clearItems()
        }
       
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
    //Initiate and load map
    func setCameraOnMap () {
        if markerArr.count > 0{
            let marker = markerArr.first
            let camera = GMSCameraPosition.camera(withLatitude: (marker?.position.latitude)!, longitude: (marker?.position.longitude)!, zoom: 15.0)
            mapContainerView.camera = camera
            mapContainerView.isMyLocationEnabled = false
            
            
            //Add markers on map and Move camera to show complete region over map
            var bounds = GMSCoordinateBounds()
            for marker in markerArr {
                bounds = bounds.includingCoordinate(marker.position)
            }
            
            let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
            mapContainerView.animate(with: update)
        }
    }
    
    func resizeViews(){
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.handleReload), userInfo: nil, repeats: false)
    }
    
    @objc func handleReload(){
        self.photoStackViewHeight.constant = self.mediaArray.count > 0 ?  self.photosCollectionView.collectionViewLayout.collectionViewContentSize.height + 40 : 0
        self.tableViewHeightCons.constant = self.feedTableView.contentSize.height
    }
    
}

extension NearbyController : GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate{
    // MARK: - GMUClusterManagerDelegate
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: mapContainerView.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapContainerView.moveCamera(update)
        return true
    }
    
    // MARK: - GMUMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            let model = self.nearbyEvents.filter({$0.postTitle == poiItem.name})[0]
            self.detailedView.isEventType = model.postType == "event" ? true : false
            self.detailedView.postModel = model
            self.detailedView.currentController = self
            self.detailedView.initializeView(frame: self.view.frame)
            
            self.detailedView.alpha = 0.0
            self.view.addSubview(self.detailedView)
            UIView.animate(withDuration: 0.5) { self.detailedView.alpha = 1.0 }
            
        } else {
            print("Did tap a normal marker")
        }
        return false
    }
    
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        let marker = GMSMarker()
        if object is POIItem {
            //Set image view for gmsmarker
            let item = object as? POIItem
            marker.title = item?.name
            
            let imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 50.0, height: 50.0))
            imageView.layer.cornerRadius = 25.0
            imageView.clipsToBounds = true
            imageView.layer.borderWidth = 1.0
            imageView.layer.borderColor = UIColor.white.cgColor
            //if item?.image != nil{
                imageView.downloadImage(url: item?.image, placeholder: "placeholder")
//            }else{
//                imageView.image = item?.thumb
//            }
            marker.iconView = imageView
            
            return marker
        }
        return nil
    }
}

extension NearbyController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        //show places from google place api.
        self.openPlaces()
        locationCallback = { address in
            self.updatedAddress = address
            searchBar.text = address.address
            self.searchAPI(lat: address.lat, long: address.lng, interests: self.selectedInterestStrings, maxDistance: self.maximumDistance, ageRange: self.ageCategory)
        }
        self.view.crossDissolveAnimation(duration: 0.2)
        return false
    }
}

class POIItem: NSObject, GMUClusterItem {
    var position = CLLocationCoordinate2D()
    var name: String! = ""
    var image: String = ""
    var thumb = UIImage()
    
    init(position: CLLocationCoordinate2D, name: String,thumb:UIImage) {
        self.thumb = thumb
    }
    
    init(position: CLLocationCoordinate2D, name: String, image: String) {
        self.position = position
        self.name  = name
        self.image = image
        
    }
    
}

extension NearbyController : UICollectionViewDelegate, UICollectionViewDataSource,ContentDynamicLayoutDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView === interestCollectionView ? interestArray.count : mediaArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView === photosCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PostMediaCell
            //PostMediaCell
            let model = mediaArray[indexPath.row]
            cell.videoBtn.isHidden = model.isImage ? true : false
            cell.thumbImg.downloadImage(url: model.uploadedUrl, placeholder: "placeholder")
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! customCollectionCell
        cell.interestBtn.setTitle(interestArray[indexPath.row].name, for: .normal)
        
        let imageName = interestArray[indexPath.row].isSelected == true ? interestArray[indexPath.row].name + "-interested" : "gray-interested"
        cell.interestBtn.setBackgroundImage(UIImage(named: imageName), for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView === photosCollectionView{
            if mediaArray[indexPath.row].isImage{
                //its an image
                let cell           = collectionView.cellForItem(at: indexPath) as! PostMediaCell
                self.performZoomInForStartingImageView(cell.thumbImg)
            }else{
                //its a video.
                let videosPlayVC = storyboard?.instantiateViewController(withIdentifier: "VideosPlayVC") as! VideosPlayVC
                navigationController?.pushViewController(videosPlayVC, animated: true)
            }
        }
    }
    
    func cellSize(indexPath: IndexPath) -> CGSize {
        return cellsSizes[indexPath.row]
    }

}

extension NearbyController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearbyEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellId") as! PostCell
       // cell.joinBtn.addTarget(self, action: #selector(joinPressed(_:)), for: .touchUpInside)
        //cell.interestedBtn.addTarget(self, action: #selector(interestedPressed(_:)), for: .touchUpInside)
       // cell.likeBtn.addTarget(self, action: #selector(likePressed(_:)), for: .touchUpInside)
        
        cell.bindData(nearbyEvents[indexPath.row], myFeed: true)
        
        
        if indexPath.row == nearbyEvents.count-1{
            // Insert the code here
            UIView.animate(withDuration: 0, animations: {
                self.feedTableView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.feedTableView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                heightOfTableView += cell.frame.height
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.tableViewHeightCons.constant = heightOfTableView
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
