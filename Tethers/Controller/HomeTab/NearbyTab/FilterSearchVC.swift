//
//  FilterSearchVC.swift
//  Tethers
//
//  Created by signity on 06/02/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit

enum NavigateFrom {
    case discover
    case myFeed
}

class FilterSearchVC: UIViewController {

    // MARK:- Outlets.
    @IBOutlet weak var filterTableView: UITableView!
    var sliderProgress : Int?
    var ageCatagory    : String?
    var comeFrom       : NavigateFrom?
    
    // MARK:- Life Cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    // MARK:- NavBar setup.
    func setupNavigationBar(){
        navigationController?.isNavigationBarHidden = false
        self.title = "Filters"
        let rightButtonItem = UIBarButtonItem.init(title: "Done",style:.plain,target: self,action: #selector(rightButtonAction(sender:)))
        rightButtonItem.tintColor = UIColor(red: 231/255, green: 59/255, blue: 91/255, alpha: 1.0)
        
        let button: UIButton = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_back-pink"), for: .normal)
        button.addTarget(self, action: #selector(leftButtonAction(sender:)), for: .touchUpInside)
        let leftButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.leftBarButtonItem  = leftButtonItem
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    // MARK:- Done button tapped.
    @objc func rightButtonAction(sender: UIBarButtonItem){
        filterResponse!(sliderProgress,ageCatagory)
        navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Left button tapped.
    @objc func leftButtonAction(sender: UIBarButtonItem){
        navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Move Slider.
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: filterTableView)
        let indexPath      = filterTableView.indexPathForRow(at: buttonPosition)
        let cell           = filterTableView.cellForRow(at: indexPath!) as? DistanceCell
        sliderProgress = Int(sender.value)
        cell?.sliderProgressLabel.text = String(format: "%d Kms", sliderProgress!)
    }
    
    // MARK:- Change Gender.
    @IBAction func genderChanged(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: filterTableView)
        let indexPath      = filterTableView.indexPathForRow(at: buttonPosition)
        let cell           = filterTableView.cellForRow(at: indexPath!) as? GenderCell
        
        cell?.maleBtn.isSelected  = false
        cell?.femaleBtn.isSelected = false
        sender.isSelected = !sender.isSelected
    }
   
    // MARK:- Age Catagory changed.
    @IBAction func ageCatagoryChanged(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint(x: 0, y: 0), to: filterTableView)
        let indexPath      = filterTableView.indexPathForRow(at: buttonPosition)
        let cell           = filterTableView.cellForRow(at: indexPath!) as? AgeRestrictionCell
        
        cell?.allBtn.isSelected  = false
        cell?.above18.isSelected = false
        cell?.below18.isSelected = false
        
        sender.isSelected = !sender.isSelected
        switch sender.tag {
        case 0:
            ageCatagory = "all"
            break
        case 1:
            ageCatagory = "below18"
            break
        default:
            ageCatagory = "above18"
            break
        }
    }
}

extension FilterSearchVC: UITableViewDelegate,UITableViewDataSource{
    // MARK:- TableView delegates and DataSource methods.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DistanceCellId", for: indexPath) as! DistanceCell
            let progress = Float(sliderProgress!)
            cell.distanceSlider.setValue(progress, animated: true)
            cell.sliderProgressLabel.text = String(format: "%d Kms", Int(progress))
            return cell
        } else if indexPath.row == 1 {
            return tableView.dequeueReusableCell(withIdentifier: "GenderCellId", for: indexPath) as! GenderCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgeRangeCellId", for: indexPath) as! AgeRestrictionCell
            cell.allBtn.isSelected = false
            cell.below18.isSelected = false
            cell.above18.isSelected = false
            
            switch ageCatagory{
            case "all":
                cell.allBtn.isSelected = true
                break
            case "above18":
                cell.above18.isSelected = true
                break
            default:
                cell.below18.isSelected = true
                break
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (comeFrom == .discover && indexPath.row == 1) ? 0 : UITableView.automaticDimension
    }
    
}
