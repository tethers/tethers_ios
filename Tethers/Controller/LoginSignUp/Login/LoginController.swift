//
//  LoginController.swift
//  Tethers
//
//  Created by Pooja Rana on 29/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    // MARK: -  IBOutlet 
    @IBOutlet weak var emailTxtField: DesignableTextField!
    @IBOutlet weak var passwordTxtField: DesignableTextField!

    // MARK: -  UIButton Action 
    @IBAction func socialFBLoginAction(_ sender: Any) {
        LoginViaFBModel.shared.viaGraphAPI(sender: self, success: { response in
            print(response)
            let userName = (response["name"] as? String)! + " " + (response["last_name"] as? String)!
            
            let param = GetParamModel.shared.getSignUpParam(userName, "social", "", response["email"] as? String, "", "facebook", response["id"] as? String)
            self.loginAPI(isSocialLogin: true, parameters: param!)
            
        }) { error in
            print(error)
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if validateLogin() {
            let param = GetParamModel.shared.getLogInParam(self.emailTxtField.text!, self.passwordTxtField.text!, "")
            loginAPI(isSocialLogin: false, parameters: param!) }
    }

    // MARK: -  API Call 
    func loginAPI(isSocialLogin: Bool, parameters:[String:Any]) {
        self.view.crossDissolveAnimation(duration: 0.2)
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseRoleID") as? ChooseRoleController
        nextVC?.params       = parameters
        nextVC?.isSocial     = isSocialLogin
        self.navigationController?.pushViewController(nextVC!, animated: false)
    }
}
