//
//  ForgotPasswordController.swift
//  Tethers
//
//  Created by Pooja Rana on 29/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ForgotPasswordController: UIViewController {

    // MARK: -  IBOutlet 
    @IBOutlet weak var emailTxtField: DesignableTextField!
    
    // MARK: -  IBOutlet Action 
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if validateForgotPassword() { forgotPasswordAPI() }
    }
    
    // MARK: -  API Call 
    func forgotPasswordAPI() {
        //API Call
        let params = GetParamModel.shared.getForgotPasswordParam(emailTxtField.text!)
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.forgotPassword, parameters: params, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { result in
                let data = result as? [String: Any]
                self.showAlert(alertMessage: data?["message"] as? String ?? "", buttonTitles: ["Ok"], style: .alert, action:{ _ in
                    self.navigationController?.popViewController(animated: true)
                })
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action: nil)
        })
    }
}
