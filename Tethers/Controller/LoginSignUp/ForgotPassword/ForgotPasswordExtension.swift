//
//  ForgotPasswordExtension.swift
//  Tethers
//
//  Created by Pooja Rana on 30/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import Foundation

extension ForgotPasswordController {
    
    func validateForgotPassword() -> Bool {
        
        self.view.endEditing(true)
        if emailTxtField.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterEmail, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if (emailTxtField.text?.isNotValidateEmailId())! {
            self.showAlert(alertMessage: ConstantModel.shared.message.validEmail, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else {
            return true
        }
    }
}
