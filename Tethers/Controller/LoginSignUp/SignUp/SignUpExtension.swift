//
//  SignUpExtension.swift
//  Tethers
//
//  Created by Pooja Rana on 30/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import Foundation

extension SignUpController {
    
    func validateSignUp() -> Bool {
        
        self.view.endEditing(true)
        if userNameTxtField.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterName, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if emailTxtField.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterEmail, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if (emailTxtField.text?.isNotValidateEmailId())! {
            self.showAlert(alertMessage: ConstantModel.shared.message.validEmail, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if passwordTxtField.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.enterPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if passwordTxtField.text!.isNotValidPassword() {
            self.showAlert(alertMessage: ConstantModel.shared.message.validPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if confirmPasswordTxtField.text!.isStringEmpty() {
            self.showAlert(alertMessage: ConstantModel.shared.message.confirmPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
            
        } else if passwordTxtField.text != confirmPasswordTxtField.text {
            self.showAlert(alertMessage: ConstantModel.shared.message.matchPassword, buttonTitles: ["Ok"], style: .alert, action: nil)
            return false
        
        } else {
            return true
        }
    }
}
