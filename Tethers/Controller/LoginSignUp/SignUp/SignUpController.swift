//
//  SignUpController.swift
//  Tethers
//
//  Created by Pooja Rana on 29/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {

    // MARK: -  IBOutlet 
    @IBOutlet weak var userNameTxtField: DesignableTextField!
    @IBOutlet weak var emailTxtField: DesignableTextField!
    @IBOutlet weak var passwordTxtField: DesignableTextField!
    @IBOutlet weak var confirmPasswordTxtField: DesignableTextField!

    @IBOutlet weak var indicatorXOrigin: NSLayoutConstraint!
    @IBOutlet weak var indicatorWidth: NSLayoutConstraint!
    @IBOutlet weak var btnuserRole: UIButton!
    @IBOutlet weak var btnOrganiserRole: UIButton!
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        btnuserRole.isSelected = true
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func signUpAction(_ sender: Any) {
        if validateSignUp() { signUpAPI() }
    }
    
    @IBAction func roleButtonAction(_ sender: UIButton) {
        sender.tag == 1 ? changeRole(user: true, x: 17.0, width: 38.0) : changeRole(user: false, x: 79.0, width: 117.0)
    }
    
    func changeRole(user: Bool, x: CGFloat, width: CGFloat) {
        self.btnuserRole.isSelected = user
        self.btnOrganiserRole.isSelected = !user
        self.indicatorXOrigin.constant = x
        self.indicatorWidth.constant   = width
    }
    
    // MARK: -  API Call 
    func signUpAPI() {
        let param = GetParamModel.shared.getSignUpParam(userNameTxtField.text!, "signup", (btnuserRole.isSelected ? "user" : "organizer"), emailTxtField.text!, passwordTxtField.text!, "", nil)
        
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.userRegister, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseLoginSignupResponse(response: response, sender: self, callback: { result in
                self.performSegue(withIdentifier: "showInterestFromSignUp", sender: nil)
            })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}
