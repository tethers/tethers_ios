//
//  DetailedView.swift
//  Tethers
//
//  Created by signity on 04/02/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit

class DetailedView: UIView {
    //Outlets.
    @IBOutlet var detailOuterView: UIView!
    @IBOutlet weak var detailView: DesignableView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var profileImage: DesignableImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var postEventName: UILabel!
    @IBOutlet weak var postEventDesc: UILabel!
    @IBOutlet weak var postEventDateTime: UILabel!
    @IBOutlet weak var eventPeopleAttending: UILabel!
    @IBOutlet weak var eventTicketsAvailable: UILabel!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var joinInterestBtn: DesignableButton!
    @IBOutlet weak var peopleAttendingStackView: UIStackView!
    @IBOutlet weak var ticketAvailableStackView: UIStackView!
    @IBOutlet weak var dateTimeImg: UIImageView!
    @IBOutlet weak var chatBtn: UIButton!
    
    
    var isEventType = false
    var postModel   = PostModel()
    var mediaArr    = [MediaModel]()
    var currentController : UIViewController!
    
    @IBAction func crossBtnAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func chatBtnAction(_ sender: Any) {
        self.removeFromSuperview()
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.user = postModel.postCreator
        currentController.navigationController?.pushViewController(chatLogController, animated: true)
    }
    
    @IBAction func shareBtnAction(_ sender: Any) {
        let image = UIImage(named: "placeholder")
        
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop]
        // present the view controller
        currentController.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func interestedJoinBtnAction(_ sender: UIButton) {
        if sender.isSelected == false {
            currentController.joinInterestAPI(param: GetParamModel.shared.joinInterestParam(postId: postModel.postId, type: isEventType ? "interest" : "join")!)
            sender.setTitle(isEventType ? "interested" : "joining", for: .normal)
            sender.isSelected = !sender.isSelected
            if isEventType{
                postModel.userInterested = sender.isSelected
            }else{
                postModel.userJoined = sender.isSelected
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("DetailedView", owner: self, options: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Reverse geocoding.
    func getAddressFromCoordinates(latitude: String,longitude: String) -> String{
        
        let geocoder = CLGeocoder()
        let center = CLLocationCoordinate2D(latitude: Double("\(latitude)")!, longitude: Double("\(longitude)")!)
        
        let loc = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString = ""
        
        geocoder.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil){
                    print("reverse geocode fail: \(error!.localizedDescription)")
                }
                
                let placemarks = placemarks! as [CLPlacemark]
                if placemarks.count > 0 {
                    let placemark = placemarks[0]
                    if placemark.subLocality != nil {
                        addressString = addressString + placemark.subLocality! + ", "
                    }
                    if placemark.locality != nil {
                        addressString = addressString + placemark.locality! + ", "
                    }
                    if placemark.country != nil {
                        addressString = addressString + placemark.country!
                    }
                    self.postEventDateTime.text = ", " + addressString
            }
        })
        return addressString
    }


    func initializeView(frame:CGRect){
        self.frame = frame
        detailOuterView.frame = frame
        self.addSubview(detailOuterView)
        
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        photosCollectionView.register(UINib(nibName: "DetailCollectionPhotoCell", bundle: nil), forCellWithReuseIdentifier: "DetailCollectionPhotoCell")
        
        //initialize things.
        //get media.
        mediaArr.removeAll()
        if postModel.media != nil {
            mediaArr.append(contentsOf: postModel.media!)
            photosCollectionView.reloadData()
        }
        
        
        //profile image of creator.
        profileImage.downloadImage(url: postModel.postCreator?.profilePic_url, placeholder: "placeholder")
        
        //distance label text.
        if postModel.taggedLocation != nil {
            let currentLatLng = CLLocation(latitude: Double(ConstantModel.shared.currentAddress.lat) ?? 0.0, longitude: Double(ConstantModel.shared.currentAddress.lng) ?? 0.0)
            let postLatLng = CLLocation(latitude: Double(postModel.taggedLocation!.lat) ?? 0.0, longitude: Double(postModel.taggedLocation!.lng) ?? 0.0)
            let distance = currentLatLng.distance(from: postLatLng)/1000
            distanceLabel.text = String(format: "%.01f km", distance)
        } else {
            distanceLabel.text = ""
        }
        
        //post/event name
        postEventName.text = postModel.postCreator?.display_name
        
        //post/event description.
        postEventDesc.text = isEventType ? postModel.postDescription : postModel.postTitle
        
        //image
        //resturant-location-icon
        let imageReqForIcon = isEventType ? "calendar-round" : "resturant-location-icon"
        dateTimeImg.image = UIImage(named: imageReqForIcon)
        
        //post/event date,time,location.
        if isEventType{
            postEventDateTime.text = postModel.eventDate + ", " +  postModel.eventTime
        }else{
            postEventDateTime.text = (postModel.postCreateDate != nil ? postModel.postCreateDate!.getElapsedInterval() : "") + ((postModel.taggedLocation?.address != nil) ? (postModel.taggedLocation?.address)! : getAddressFromCoordinates(latitude: postModel.current_lat, longitude: postModel.current_long))
        }
        
        //event people attending
        let peopleAttendingArrCount = postModel.joinedInteresed?.filter({$0.type == "interest"}).count
        eventPeopleAttending.text = "\(peopleAttendingArrCount!) people attending"
        //eventTicketsAvailable.text = "qj34hfrjhq3w4klefj"
        //noteLabel.text = "wek4htukg2ghw3f"
        
        chatBtn.isHidden = UserDefaultsHandler.userInfo?.profile_id == postModel.postCreator?.profile_id ? true : false
        
        let imageReq = isEventType ? "interestedButton" : "joinButton"
        let textReq  = isEventType ? "Interested"       : postModel.userJoined ? "Joining" : "Join"
        joinInterestBtn.setBackgroundImage(UIImage(named: imageReq), for: .normal)
        joinInterestBtn.setTitle(textReq, for: .normal)
        joinInterestBtn.isSelected = isEventType ? postModel.userInterested : postModel.userJoined
        
        profileImage.layer.cornerRadius = isEventType ? 20.0 : profileImage.frame.size.height/2.0
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileDetailAction(_sender:))))
        peopleAttendingStackView.isHidden = isEventType ? false : true
        ticketAvailableStackView.isHidden = true
        noteView.isHidden = true
        
    }
    
    @objc func profileDetailAction(_sender:Any){
        //Go to other screen here.
        self.removeFromSuperview()
        let storyBoard =  UIStoryboard(name: "Home", bundle: nil)
        let othersProfileController = storyBoard.instantiateViewController(withIdentifier: "OthersProfileController") as! OthersProfileController
        othersProfileController.model = postModel.postCreator
        currentController.navigationController?.pushViewController(othersProfileController, animated: true)
    }
    
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        photosCollectionView.scrollRectToVisible(CGRect(x: Int(photosCollectionView.frame.size.width) * sender.currentPage,y: 0,width:Int(photosCollectionView.frame.size.width),height: Int(photosCollectionView.frame.size.height)), animated: true)
    }
}

extension DetailedView : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = mediaArr.count
        pageControl.isHidden = !(mediaArr.count > 1)
        return mediaArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailCollectionPhotoCell", for: indexPath) as! DetailCollectionPhotoCell
        cell.detailImg.downloadImage(url: mediaArr[indexPath.row].uploadedUrl, placeholder: "placeholder")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.photosCollectionView.frame.size.width, height: self.photosCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //play the videos or show the images.
        if postModel.media![indexPath.row].isImage{
            //its an image
            let cell           = collectionView.cellForItem(at: indexPath) as! DetailCollectionPhotoCell
            currentController.performZoomInForStartingImageView(cell.detailImg)
        }else{
            //its a video.
            self.removeFromSuperview()
            let videosPlayVC = currentController.storyboard?.instantiateViewController(withIdentifier: "VideosPlayVC") as! VideosPlayVC
            videosPlayVC.media = postModel.media![indexPath.row]
            currentController.navigationController?.pushViewController(videosPlayVC, animated: true)
        }
    }
    
}
