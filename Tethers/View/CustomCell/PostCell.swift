//
//  PostCell.swift
//  Tethers
//
//  Created by Pooja Rana on 27/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class PostCell : UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var postTime: UILabel!
    @IBOutlet weak var postDistance: UILabel!
    @IBOutlet weak var postTitle: UILabel!
    
    @IBOutlet weak var joinBtn: DesignableButton!
    @IBOutlet weak var joinBtnWidth: NSLayoutConstraint!
    
    @IBOutlet weak var postMediaCollection: UICollectionView!
    @IBOutlet weak var postMediaHgt: NSLayoutConstraint!
    var mediaArr: [MediaModel]?
    var taggedLoc: AddressModel?

    @IBOutlet weak var locationLbl: UILabel!
    
    //Event Info
    @IBOutlet weak var eventInfoViewHgt: NSLayoutConstraint!
    @IBOutlet weak var eventDecrip: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventLoc: UILabel!
    @IBOutlet weak var peopleInterestedLbl: UILabel!
    @IBOutlet weak var interestedBtn: DesignableButton!
    
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var totalLikesBtn: UIButton!
    @IBOutlet weak var totalCommentsBtn: UIButton!
    @IBOutlet weak var likeCommentViewHgt: NSLayoutConstraint!
    
    func bindData(_ model: PostModel, myFeed: Bool) {
        
        if myFeed {
            self.userImg.downloadImage(url: model.postCreator?.profilePic_url, placeholder: "placeholder")
            self.userName.text = model.postCreator?.display_name
            if model.taggedLocation != nil {
                let currentLatLng = CLLocation(latitude: Double(ConstantModel.shared.currentAddress.lat) ?? 0.0, longitude: Double(ConstantModel.shared.currentAddress.lng) ?? 0.0)
                let postLatLng = CLLocation(latitude: Double(model.taggedLocation!.lat) ?? 0.0, longitude: Double(model.taggedLocation!.lng) ?? 0.0)
                let distance = currentLatLng.distance(from: postLatLng)/1000
                self.postDistance.text = String(format: "%.01fkm", distance)
                
            } else {
                self.postDistance.text = ""
            }
            
            self.postTime.text = model.postCreateDate == nil ? "Just now" : model.postCreateDate!.getElapsedInterval()
            self.postTime.textColor = self.postTime.text == "Just now" ? UIColor.red : UIColor(red: 162.0/255.0, green: 164.0/255.0, blue: 171.0/255.0, alpha: 1)
        }
        
        
        self.postTitle.text = model.postTitle
        self.joinBtnWidth.constant = model.canJoin ? 78.0 : 0.0
        self.joinBtn.isSelected = model.userJoined
        
        self.mediaArr = model.media
        self.taggedLoc = model.taggedLocation
        
        self.postMediaCollection.reloadData()
        self.postMediaHgt.constant = ((model.media != nil && model.media!.count > 0) || model.taggedLocation != nil) ? 210.0 : 0.0
        
        //Event Handling
        if model.postType == "event" {
            self.eventInfoViewHgt.constant = 118.0
            self.locationLbl.text = ""
            
            self.eventDecrip.text = model.postDescription
            self.eventDate.text = model.eventDate
            self.eventLoc.text = String(format: "%@, %@", model.eventTime, model.taggedLocation?.address ?? "")
            self.peopleInterestedLbl.text = ""
            self.interestedBtn.isSelected = model.userInterested
            
        } else {
            self.eventInfoViewHgt.constant = 0.0
            self.locationLbl.text = model.taggedLocation?.address
        }
        self.setNeedsUpdateConstraints()
        self.layoutIfNeeded()
        
        self.likeCommentViewHgt.constant = model.allowCommentLike ? 51.0 : 0.0
        if self.likeCommentViewHgt.constant != 0.0 {
            self.likeBtn.isSelected = model.userLiked
            
            if model.totalLikes > 0 {
                self.totalLikesBtn.isHidden = false
                let likes = String(format: "%d %@", model.totalLikes, (model.totalLikes == 1 ? "like" : "likes"))
                self.totalLikesBtn.setTitle(likes, for: .normal)
                
            } else {
                self.totalLikesBtn.isHidden = true
            }
            
            if model.totalComment > 0 {
                self.totalCommentsBtn.isHidden = false
                let comments = String(format: "%d %@", model.totalComment, (model.totalComment == 1 ? "comment" : "comments"))
                self.totalCommentsBtn.setTitle(comments, for: .normal)
                
            } else {
                self.totalCommentsBtn.isHidden = true
            }
        }
    }
}

class PostMediaCell : UICollectionViewCell {
    @IBOutlet weak var thumbImg: UIImageView!
    @IBOutlet weak var videoBtn: UIButton!
}

extension PostCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: -  UICollectionView Delegate, DelegateFlowLayout and Data Source 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (mediaArr != nil && mediaArr!.count > 0) ? mediaArr!.count : (taggedLoc != nil ? 1 : 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width =  (mediaArr != nil && mediaArr!.count > 1) ? UIScreen.main.bounds.size.width-50 : UIScreen.main.bounds.size.width
        return CGSize(width: width, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostMediaCellId", for: indexPath) as! PostMediaCell
        
        let url = (mediaArr != nil && mediaArr!.count > 0) ? mediaArr![indexPath.row].uploadedUrl : ("http://maps.google.com/maps/api/staticmap?markers=\(Float(taggedLoc?.lat ?? "0.0")!),\(Float(taggedLoc?.lng ?? "0.0")!)&\("zoom=15&size=600x300")&sensor=true&key=\(ConstantModel.shared.GOOGLE_API_KEY)")
        
        cell.videoBtn.isHidden = (mediaArr != nil && mediaArr!.count > 0) ? (mediaArr![indexPath.row].isImage ? true : false) : true
        
        cell.thumbImg.downloadImage(url: url, placeholder: "placeholder")
        return cell
    }
}

class NearestEventCell : UITableViewCell {
    @IBOutlet weak var nearByEventsCollection: UICollectionView!
}
