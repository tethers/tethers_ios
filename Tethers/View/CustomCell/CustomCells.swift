//
//  CustomCells.swift
//  Tethers
//
//  Created by Pooja Rana on 16/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import AVKit

// MARK: -  Choose Interest 
class ChooseInterestCell: UICollectionViewCell {
    @IBOutlet weak var interestImg: UIButton!
    @IBOutlet weak var interestLbl: UILabel!
}

class DetailCollectionPhotoCell: UICollectionViewCell {
    @IBOutlet weak var detailImg: UIImageView!
}


// MARK: -  Upload Post 
class FilterCell : UICollectionViewCell {
    @IBOutlet weak var filterImg: UIImageView!
}

class FeedDescriptionCell : UITableViewCell {
    @IBOutlet weak var feedDescTxt: DesignableTextView!
    @IBOutlet weak var peopleLocLbl: UILabel!
}

class FeedActionItemCell : UITableViewCell {
    @IBOutlet weak var tagViewHgt: NSLayoutConstraint!
    @IBOutlet weak var tagListView: TagListView!
}

class FeedMediaCell : UITableViewCell {
    @IBOutlet weak var mediaCaptionTxt: DesignableTextView!
    @IBOutlet weak var captionHgt: NSLayoutConstraint!
    @IBOutlet weak var moreActionBtn: UIButton!
    
    @IBOutlet weak var mediaThumb: UIImageView!
    @IBOutlet weak var videoPlayIcon: UIButton!
    @IBOutlet weak var playerView: PlayerView!
    
    func bindData(_ model: MediaModel) {

        self.mediaCaptionTxt.placeholder  = model.isImage ? "About this photo..." : "About this video..."
        self.mediaCaptionTxt.text = model.caption
        
        if let img = model.thumb {
            self.mediaThumb.image = EventManager.resizeImg(image: img, size: self.mediaThumb.frame.size)
        }
        
        if model.isImage == false {
            self.playerView?.isHidden    = false
            self.videoPlayIcon.isHidden  = false
            self.mediaThumb.isHidden     = true
            
            if let url = model.mediaUrl {
                let avPlayer = AVPlayer(url: url)
                self.playerView?.playerLayer.player = avPlayer;
                self.playerView?.playerLayer.videoGravity = .resizeAspectFill
            }
            
        } else {
            self.playerView?.isHidden    = true
            self.videoPlayIcon.isHidden  = true
            self.mediaThumb.isHidden     = false
            
            if let img = model.thumb {
                self.mediaThumb.image = EventManager.resizeImg(image: img, size: self.mediaThumb.frame.size)
            }
        }
    }
}

class AgeRestrictionCell : UITableViewCell {
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var below18: UIButton!
    @IBOutlet weak var above18: UIButton!
    
    func bindData(_ model: PostModel) {
        self.allBtn.isSelected = model.ageCategory == "all" ? true : false
        self.below18.isSelected = model.ageCategory == "below18" ? true : false
        self.above18.isSelected = model.ageCategory == "above18" ? true : false
    }
}

class GenderCell : UITableViewCell {
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
}

class DistanceCell : UITableViewCell {
    
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var sliderProgressLabel: UILabel!
}

class JoinPostCell : UITableViewCell {
    @IBOutlet weak var joinSwitch: UISwitch! {
        didSet {
            switchSettings(switchBtn: joinSwitch)
        }
    }
    
    @IBOutlet weak var allowCommentsLikes: UISwitch! {
        didSet {
            switchSettings(switchBtn: allowCommentsLikes)
        }
    }
    
    func switchSettings(switchBtn: UISwitch)  {
        let onColor  = UIColor.AppThemeColor
        let offColor = UIColor(red: 218/255.0, green: 221/255.0, blue: 228/255.0, alpha: 1)
        
        /*For on state*/
        switchBtn.onTintColor = onColor
        /*For off state*/
        switchBtn.tintColor = offColor
        
        switchBtn.layer.cornerRadius = switchBtn.frame.height / 2
        switchBtn.backgroundColor = offColor
    }
    
    func bindData(_ model: PostModel) {
        self.joinSwitch.isOn = model.canJoin
        self.allowCommentsLikes.isOn = model.allowCommentLike
    }
}

// MARK: -  My Profile 
class MyPostsEventsCell : UITableViewCell {
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var eventPictureImageView: UIImageView!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventLocationLabel: UILabel!
    @IBOutlet weak var joiningButton: UIButton!
    
    func bindData(_ model: PostModel) {        
        self.dayLbl.text = model.postCreateDate?.getDetailedElapseTime()
        
        if model.media != nil && model.media!.count > 0 {
            let firstMedia = model.media![0]
            self.eventPictureImageView.downloadImage(url: firstMedia.uploadedUrl, placeholder: "placeholder")
        }
        self.eventNameLabel.text = model.postTitle
        self.eventLocationLabel.text = model.taggedLocation?.address
    }
}

class FollowFollowingCell : UITableViewCell {
    @IBOutlet weak var userImg: DesignableImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userRelation: DesignableButton!
}

// MARK: -  Others Profile 
class UserGalleryCollectionCell : UICollectionViewCell{
    @IBOutlet weak var picImageView: UIImageView!
}

class LogoutCell: UITableViewCell {
    @IBAction func logOutAction(_ sender: Any) {
        UserDefaultsHandler.pageToShow = ""
        NavigationManager.shared.checkWhereToNav()
    }
}

// MARK: -  Post 
class LikeCell : UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    func bindData(_ model: LikeCommentModel) {
        self.userImg.downloadImage(url: model.user?.profilePic_url, placeholder: "placeholder")
        self.userName.text = model.user?.display_name
    }
    
    func bindData(_ model: ProfileModel) {
        self.userImg.downloadImage(url: model.profilePic_url, placeholder: "placeholder")
        self.userName.text = model.display_name
    }
}

class CommentCell : UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var timeAgoLbl: UILabel!
    
    func bindData(_ model: LikeCommentModel) {
        self.userImg.downloadImage(url: model.user?.profilePic_url, placeholder: "placeholder")        
        self.userNameLbl.text = model.user?.display_name
        self.commentLbl.text  = model.comment
        self.timeAgoLbl.text  = model.createDate == nil ? "Just now" : model.createDate!.getElapsedInterval()
        self.timeAgoLbl.textColor = self.timeAgoLbl.text == "Just now" ? UIColor.red : UIColor(red: 166.0/255.0, green: 169.0/255.0, blue: 178.0/255.0, alpha: 1)
    }
}

class customCollectionCell : UICollectionViewCell{
    @IBOutlet weak var interestBtn: UIButton!
    @IBOutlet weak var photoImageView: UIImageView!
    
}
