
import UIKit

@IBDesignable public class DesignableLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 15, left: 15, bottom: 15, right: 15)
        super.drawText(in: rect.inset(by: insets))
    }
}
