

import UIKit
private var __maxLengths = [UITextField: Int]()

@IBDesignable public class DesignableTextField: UITextField, UITextFieldDelegate {
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    @IBInspectable var leftAddView: CGRect = CGRect.zero
    @IBInspectable var leftimageView: CGRect = CGRect.zero
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.delegate = self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @IBInspectable public var placeholderColor: UIColor = UIColor.clear {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder!, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
            layoutSubviews()
            
        }
    }
    
    @IBInspectable public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var SideImage:UIImage? {
        didSet {
            
            let leftAddView = UIView(frame: self.leftAddView)
            let leftimageView = UIImageView(frame: self.leftimageView)
            leftimageView.image = SideImage
            leftAddView.addSubview(leftimageView)
            
            self.leftView = leftAddView
            self.leftViewMode = UITextField.ViewMode.always
        }
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
   
    @IBInspectable public var lineHeight: CGFloat = 0 {
        didSet {
            self.borderStyle = .none
            self.layer.backgroundColor = UIColor.white.cgColor
            
            self.layer.masksToBounds = false
            
            self.layer.shadowColor =  UIColor.textfieldDividerColor.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }
    
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
    
//    @IBInspectable internal var maxLength: Int = 0
    
    // MARK: - Delegates
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        let newLength = currentCharacterCount + string.count - range.length
        
        if textField.isSecureTextEntry {
            if (string == " ") {
                return false
            }
        }
        
        if maxLength != 0 {
            return newLength <= maxLength
        } else {
            return true
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage      = textField.tag+1
        // Try to find next responder
        let nextResponder = textField.superview?.viewWithTag(nextTage)
        if (nextResponder != nil) {
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
            
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return true // We do not want UITextField to insert line-breaks.
    }
}
