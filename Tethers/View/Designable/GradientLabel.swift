//
//  GradientLabel.swift
//  Tethers
//
//  Created by Signity on 24/04/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class GradientLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable var firstColor : UIColor = UIColor.white {
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
    }
    
    @IBInspectable var secondColor : UIColor = UIColor.white {
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
    }
    
    @IBInspectable var vertical : Bool = false {
        
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
    }
    
    
    
    @IBInspectable var setGradientBackground : Bool = false {
        
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
        
    }
    
    
    func applyGradient() {
        let colors = [firstColor.cgColor, secondColor.cgColor]
        
        let layer = CAGradientLayer()
        layer.colors = colors
        layer.frame = self.bounds
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        
        self.layer.addSublayer(layer)
    }
    
}
import UIKit

class GradientView: UIView {
    @IBInspectable var firstColor : UIColor = UIColor.white {
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
    }
    
    @IBInspectable var secondColor : UIColor = UIColor.white {
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
    }
    
    @IBInspectable var vertical : Bool = false {
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
    }
    
    
    
    @IBInspectable var setGradientBackground : Bool = false {
        
        didSet {
            if setGradientBackground {
                applyGradient()
            }
        }
        
    }
    
    
    func applyGradient() {
        let colors = [firstColor.cgColor, secondColor.cgColor]
        let layer = CAGradientLayer()
        layer.colors = colors
        layer.frame = self.bounds
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        self.layer.addSublayer(layer)
    }
    
}

