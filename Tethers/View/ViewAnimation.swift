//
//  ViewAnimation.swift
//  Tethers
//
//  Created by Pooja Rana on 30/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

extension UIView {
    func springAnimation(duration: TimeInterval, success: @escaping () -> Void) {
        self.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        UIView.animate(withDuration: duration,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: {
                        self.transform = .identity
        }, completion: { _ in
            success()
        })
    }
        
    func crossDissolveAnimation(duration: TimeInterval) {
        UIView.transition(with: ConstantModel.shared.appDelegate.window!, duration: duration, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }
    
    func fadeInFadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, alpha: CGFloat, completion: ((Bool) -> Void)? = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = alpha
        }, completion: completion)
    }
}
