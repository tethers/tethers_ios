//
//  NavigationManager.swift
//  Tethers
//
//  Created by signity on 17/09/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class NavigationManager: NSObject {
    
    static let shared       = NavigationManager()
    private override init() { }
    
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    
    // MARK:   Method for check navigate user 
    func checkWhereToNav() {
        if UserDefaultsHandler.pageToShow != nil {
            let pageName = UserDefaultsHandler.pageToShow
            switch pageName {
            case ConstantModel.shared.page.Interest :
                self.navToInterestView()
                break
            case ConstantModel.shared.page.TabScreen :
                self.navToTabBarView()
                break
            default:
                UserDefaultsHandler.removeAllUserDefaults()
                self.navToLogInView()
            }
        }
    }
    
    // MARK:   Method for navigate Application to DashBoard View 
    func navToLogInView() {
        let logInVC = storyBoard.instantiateViewController(withIdentifier: "LoginController") as? LoginController
        self.pushToNavigate(logInVC!)
    }
    
    // MARK:   Method for navigate Application to DashBoard View 
    func navToInterestView() {
        let myInterestVC = storyBoard.instantiateViewController(withIdentifier: "ChooseInterestController") as? ChooseInterestController
        self.pushToNavigate(myInterestVC!)
    }
    
    // MARK:   Method for navigate Application to DashBoard View 
    func navToTabBarView() {
        let tabBarController = ESTabBarController()
        tabBarController.tabBar.shadowImage = UIImage(named: "transparent")
        tabBarController.tabBar.backgroundImage = UIImage(named: "background_dark")
        
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        let nav1 = storyboard.instantiateViewController(withIdentifier: "NearByNav")
        let nav2 = storyboard.instantiateViewController(withIdentifier: "FeedsNav")
        let nav3 = storyboard.instantiateViewController(withIdentifier: "CameraNav")
        let nav4 = storyboard.instantiateViewController(withIdentifier: "ChatListNav")
        let nav5 = storyboard.instantiateViewController(withIdentifier: "MyProfileNav")
        
        nav1.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), image: UIImage(named: "discover"), selectedImage: UIImage(named: "discover-active"))
        nav2.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), image: UIImage(named: "ic_feed"), selectedImage: UIImage(named: "ic_feed-active"))
        nav3.tabBarItem = ESTabBarItem.init(ExampleIrregularityContentView(), image: UIImage(named: "ic_add-add"), selectedImage: UIImage(named: "ic_add-add"))
        nav4.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), image: UIImage(named: "ic_chat-inActive"), selectedImage: UIImage(named: "ic_chat-active"))
        nav5.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), image: UIImage(named: "ic_profile"), selectedImage: UIImage(named: "ic_profile active"))
        tabBarController.viewControllers = [nav1, nav2, nav3, nav4, nav5]
        
        ConstantModel.shared.appDelegate.window?.rootViewController = tabBarController
    }
    
    // MARK:   Method for Distance Filter View 
    func pushToNavigate(_ navToVC: UIViewController) {
        let navigationController = UINavigationController(rootViewController: navToVC)
        navigationController.navigationBar.isHidden = true
        ConstantModel.shared.appDelegate.window?.rootViewController = navigationController
        ConstantModel.shared.appDelegate.window?.makeKeyAndVisible()
    }
}
