//
//  UpdateLocationsModal.swift
//  Tethers
//
//  Created by Anisha on 02/02/18.
//  Copyright © 2018 Anisha. All rights reserved.
//

import UIKit
import CoreLocation

 class UpdateLocationsModal: NSObject, CLLocationManagerDelegate {
    
    static let shared = UpdateLocationsModal()
    private override init() { }

    // MARK: -  Determine User Current Location 
    func determineMyCurrentLocation() {
        ConstantModel.shared.appDelegate.locationManager.delegate        = self
        ConstantModel.shared.appDelegate.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        ConstantModel.shared.appDelegate.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            ConstantModel.shared.appDelegate.locationManager.startUpdatingLocation()
            
        } else {
            let sender = ConstantModel.shared.appDelegate.window?.rootViewController
            sender?.showAlert(alertMessage: ConstantModel.shared.message.locationNetError, buttonTitles: ["Ok"], style: .alert, action: nil)
        }
    }
    
    // MARK: -  Location Manager Delegate 
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLoc = locations[0] as CLLocation
        ConstantModel.shared.currentAddress.lat = String(Double(currentLoc.coordinate.latitude))
        ConstantModel.shared.currentAddress.lng = String(Double(currentLoc.coordinate.longitude))
        ConstantModel.shared.appDelegate.locationManager.stopUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
        let code = (error as NSError).code
        let sender = ConstantModel.shared.appDelegate.window?.rootViewController
        switch code {
        case CLError.network.rawValue:
            // general, network-related error
            sender?.showAlert(alertMessage: ConstantModel.shared.message.locationNetError, buttonTitles: ["Ok"], style: .alert, action: nil)
        case CLError.denied.rawValue:
            sender?.showAlert(alertMessage: ConstantModel.shared.message.locationDenied, buttonTitles: ["Ok"], style: .alert, action: nil)
        default:
            sender?.showAlert(alertMessage: ConstantModel.shared.message.locationNotFound, buttonTitles: ["Ok"], style: .alert, action: nil)
        }
    }
}
