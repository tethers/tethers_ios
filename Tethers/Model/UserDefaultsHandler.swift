//
//  UserDefaultsHandler.swift
//    Tethers
//
//  Created by Signity on 12/02/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
class UserDefaultsHandler: NSObject {

    static let memory = UserDefaults.standard
    
    static var pageToShow: String? {
        get {
            return memory.value(forKey: "LandingPageToShow") as? String
        }
        set (pageStr){
            memory.setValue(pageStr, forKey: "LandingPageToShow")
            memory.synchronize()
        }
    }
    
    static var token: String? {
        get {
            return memory.value(forKey: "token") as? String
        }
        set (userToken){
            memory.setValue(userToken, forKey: "token")
            memory.synchronize()
        }
    }
    
    static var userInfo: ProfileModel? {
        get {
            let decoded     = memory.object(forKey: "user_info") as? Data
            return decoded == nil ? nil : NSKeyedUnarchiver.unarchiveObject(with: decoded!) as? ProfileModel
        }
        set (userModel) {
            userModel == nil ? memory.removeObject(forKey: "user_info") : memory.set(NSKeyedArchiver.archivedData(withRootObject: userModel!), forKey: "user_info")
            memory.synchronize()
        }
    }
    
   // MARK: -  Remove/Clear All User Defaults 
    class func removeAllUserDefaults() {
        let domain = Bundle.main.bundleIdentifier!
        memory.removePersistentDomain(forName: domain)
        memory.synchronize()
        self.pageToShow = ""
    }
}
