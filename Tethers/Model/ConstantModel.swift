//
//  ConstantModel.swift
//  Tethers
//
//  Created by Signity on 24/04/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ConstantModel: NSObject {
    
    static let shared        = ConstantModel()
    private override init() { }

    let APP_NAME             = "Tethers"
    let appDelegate          = UIApplication.shared.delegate as! AppDelegate
    let screenSize           = UIScreen.main.bounds
    
    let GOOGLE_API_KEY       = "AIzaSyBRTeh7l1V3eHsAKe-MwrRNOyfgq06iNkc"

    let g_effectConfig       =
        ["@adjust brightness 0 @adjust contrast 1 @adjust saturation 1 @adjust sharpen 0",
         "@adjust lut edgy_amber.png",
         "@adjust lut filmstock.png",
         "@adjust lut late_sunset.png",
         "@adjust lut wildbird.png",
         "#unpack @krblend ol hehe.jpg 100",
         "@vigblend overlay 255 0 0 255 100 0.12 0.54 0.5 0.5 3",
         "@adjust exposure 0.98",
         
         "@curve B(0, 0)(68, 72)(149, 184)(255, 255) @pixblend screen 0.94118 0.29 0.29 1 20",
         "@curve R(0, 0)(71, 74)(164, 165)(255, 255) @pixblend overlay 0.357 0.863 0.882 1 40",
         "@curve R(0, 0)(149, 145)(255, 255)G(0, 0)(149, 145)(255, 255)B(0, 0)(149, 145)(255, 255) @pixblend colordodge 0.937 0.482 0.835 1 20",
         "@curve R(0, 0)(43, 77)(56, 104)(100, 166)(255, 255)G(0, 0)(35, 53)(255, 255)B(0, 0)(110, 123)(255, 212)",
         "@adjust hsv -1 -1 -1 -1 -1 -1",
         "@vigblend mix 10 10 30 255 91 0 1.0 0.5 0.5 3 @curve R(0, 31)(35, 75)(81, 139)(109, 174)(148, 207)(255, 255)G(0, 24)(59, 88)(105, 146)(130, 171)(145, 187)(180, 214)(255, 255)B(0, 96)(63, 130)(103, 157)(169, 194)(255, 255)",
         "@adjust saturation 0 @curve R(0, 49)(16, 44)(34, 56)(74, 120)(120, 185)(151, 223)(255, 255)G(0, 46)(34, 73)(85, 129)(111, 164)(138, 192)(170, 215)(255, 255)B(0, 77)(51, 101)(105, 143)(165, 182)(210, 213)(250, 229)"]
    
    let validVideoDuration   = 30
    let mediaLimit           = 5
    let pageLimit            = 10

    let api                  = API()
    let message              = AppMessage()
    let page                 = ScreenToShow()
    
    var currentAddress       = AddressModel()
}

struct API {
    //Staging
//    let baseURL              = "http://112.196.1.221:8000/"

    //Vipul Local
    let baseURL              = "http://192.168.1.243:8000/"
    let GetAPI               = "GET"
    let PostAPI              = "POST"
    let PutAPI               = "PUT"
    
    let userRegister         = "user/v1/register/user"
    let fbRegister           = "user/v1/registerwithfb/user"
    let logIn                = "user/v1/login/user"
    let forgotPassword       = "user/v1/forgot/password"
    let changePassword       = "user/v1/changepassword/user"
    let removePhoto          = "user/v1/removephoto"
    let addInterest          = "user/v1/add/interest"
    let getInterest          = "user/v1/get/interest"

    let follow               = "user/v1/follow"
    let unFollow             = "user/v1/unfollow"
    
    let getTags              = "user/v1/tags"
    let uploadPost           = "user/v1/list/post"
    let getPost              = "user/v1/get/posts"
    let getNearestEvent      = "user/v1/get/nearesteventsearch"

    let likePost             = "user/v1/add/postlike"
    let getLikes             = "user/v1/get/postLikes"
    let commentPost          = "user/v1/add/postcomment"
    let getComments          = "user/v1/get/postComments"
    let joinInterest         = "user/v1/add/postjoininterest"
    
    let getUser              = "user/v1/get/user"
    let getUserJoiningPosts  = "user/v1/get/userinterestorjoinpost"
    let getUserFollower      = "user/v1/get/userfollower"
    let getUserFollowing     = "user/v1/get/userfollowing"

    let editUser             = "user/v1/edit/user"
    let getOtherUser         = "user/v1/get/otheruser"

    let searchUser         = "user/v1/get/users?display_name="
    
    let addMediaCount      = "user/v1/add/postmediacount"
}

struct AppMessage {
    let TokenError            = "Session Expired, please try again."
    let ServiceFailure        = "Something went wrong, please try again."
    let WeakInternet          = "Please check your internet connection before using."
    let NoInternet            = "No internet connection."
    
    let comingSoon            = "Coming soon..."
    let pleaseSelect          = "Please select"
    
    let enterEmail            = "Please enter email address"
    let validEmail            = "Valid email required"
    
    let enterPassword         = "Please enter password"
    let validPassword         = "Please create a password between 8-16 characters with at least any of the following alphabet, number and allowed special characters e.g. !@#$%^&*+=?-"
    let confirmPassword       = "Please confirm password"
    let matchPassword         = "The passwords you’ve entered don’t match, please review and try again"
    
    let enterName             = "Please enter name"
    
    let selectInterest        = "Please select atleast one interest"
    
    let locationDenied        = "Location Access has been denied for Tethers! Please enable locations from settings."
    let locationNetError      = "Can't access your current location! Please check your network connection or that you are not in airplane mode!"
    let locationNotFound      = "Can't access your current location!"
    
    let cameraError1          = "Not allowed to open camera and microphone, please choose allow in the 'settings' page!!!"
    let cameraError2          = "The Camera Is Not Allowed!"

    let videoRecordInfo       = "Hold your finger on the button to record a video"
    let mediaLimitError       = "You have exceeded limit for uploading videos/images."
    let videoDurationError    = "Video more than 30 sec cannot be uploaded, Please select a small video."
    let videoNotSaveErr       = "Video not saved, error Occured"
    let imageNotSaveErr       = "Image not saved, error Occured"
    let addTitle              = "Add some description for your post"
    let NoTags                = "No tags available"
    let deleteMedia           = "Are you sure you want to delete this "

    let profileUpdate         = "Profile updated succesfully"
    let enterNewPassword      = "Please enter new password"
    let passwordUpdate        = "Password updated succesfully"
    
    let enterComment          = "Please enter comment"
    let addEventImage         = "Please add a image for event."
    let addEventTitle         = "Add title for your event"
    let addEventDescription   = "Add some description for your event"
    let addEventLocation      = "Add location of event"
    let addEventDateTime      = "Add date and time of event"
}

struct ScreenToShow {
    let Interest = "SelectInterest"
    let TabScreen = "TabBar"
}

struct DateFormats {
    static let birthDateFormat  =  "dd-MM-yyyy"
    static let eventDateFormat  =  "dd MMM yyyy"
    static let eventTimeFormat  =  "hh:mm a"

    static let uploadMediaDateFormat  =  "yyyy-MM-dd_HH:mm:ss.SSS"
    static let postDateFormat  =   "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
}

//Test ids
//Sneha ----> 5bcead0212365e140d1f6835
//Pooji ----> 5c1b4e560045a32dc43f1e8b
//Sanju ----> 5c232ffcd1a5fd30ba610af1
//Priya ----> 5c2331095f0557322d1fa7bf
