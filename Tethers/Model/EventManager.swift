//
//  EventManager.swift
//  Tethers
//
//  Created by Signity on 30/01/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import Reachability
import MBProgressHUD

class EventManager: NSObject {
    
    // MARK: -  Check for Internet 
    class func checkForInternetConnection() -> Bool {
        return ((Reachability()?.connection)! == .none) ? false : true
    }
    
    // MARK: -  Loader 
    class func showloader() {
        MBProgressHUD.showAdded(to: (ConstantModel.shared.appDelegate.window)!, animated: true)
    }
    
    class func hideloader() {
        MBProgressHUD.hide(for: (ConstantModel.shared.appDelegate.window)!, animated: true)
    }
    
    class func isValidUrl(url: String?) -> Bool {
        return (url == nil || url == "" || url == "http://gravatar.com/avatar") ? false : true
    }
    
    // MARK: -  DatePicker 
    class func inputV(_ inputVV: UIView, sender: Any?) -> UIView {
        let inputView = UIView(frame:inputVV.frame)
        inputVV.frame.size.width = UIScreen.main.bounds.width
        inputView.addSubview(inputVV)
        inputView.addSubview(toolBar(isInputAccessory: false, sender))
        return inputView
    }
    
    class func toolBar (isInputAccessory:Bool, _ target : Any?) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.AppThemeColor
        
        var items = [UIBarButtonItem]()
        if !isInputAccessory {
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.done, target: target, action: NSSelectorFromString("resignTextField:"))
            cancelButton.tag = 100
            items.append(cancelButton)
        }
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: target, action: NSSelectorFromString("resignTextField:"))
        doneButton.tag = 101
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        items.append(spaceButton)
        items.append(doneButton)

        toolBar.setItems(items, animated: false)
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
    class func resizeImg(image: UIImage, size: CGSize) -> UIImage {
        
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        
        let imageRect = CGRect(x: (size.width - width)/2.0, y: (size.height - height)/2.0, width: width, height: height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        image.draw(in: imageRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage!
    }
    
    class func getNewVideoName() -> String {
        let dateStr = Date().getDatefor(format: DateFormats.uploadMediaDateFormat)
        return String(format: "Vid_%@_%@.mp4", (UserDefaultsHandler.userInfo?.profile_id)!, dateStr)
    }
    
    class func getNewImageName() -> String {
        let dateStr = Date().getDatefor(format: DateFormats.uploadMediaDateFormat)
        return String(format: "Img_%@_%@.jpg", (UserDefaultsHandler.userInfo?.profile_id)!, dateStr)
    }
    
    class func clearFile(file: URL) {
        do {
            try FileManager.default.removeItem(at: file)
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
}
