//
//  ExtendedSearchBar.swift
//  Tethers
//
//  Created by signity on 14/02/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit

extension UISearchBar{
    func changeSearchbarColor(){
        for subView in self.subviews {
            for subViewOne in subView.subviews {
                if let textField = subViewOne as? UITextField {
                    textField.backgroundColor = UIColor.tethersCyan
                    textField.clearButtonMode = .never
                }
            }
        }
    }
}




