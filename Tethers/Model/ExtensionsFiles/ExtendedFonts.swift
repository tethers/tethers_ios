//
//  FontExtension.swift
//  Tethers
//
//  Created by Pooja Rana on 22/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func setFontLight(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Light", size: textFontSize)!
    }
    static func setFontRegular(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: textFontSize)!
    }
    
    static func setFontMedium(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Medium", size: textFontSize)!
    }
    
    static func setFontBold(textFontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: textFontSize)!
    }
}
