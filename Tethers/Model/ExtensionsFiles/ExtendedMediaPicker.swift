//
//  ExtendedMediaPicker.swift
//  Tethers
//
//  Created by Pooja Rana on 19/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import AVKit

var didfetchMedia :((UIImagePickerController, MediaModel) -> Void)?

extension UIViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func openCameraGallerySheet(isRemove: Bool, action: (() -> Void)?) {
        let actionSheetController: UIAlertController = UIAlertController(title:
            ConstantModel.shared.message.pleaseSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        if isRemove {
            actionSheetController.addAction(UIAlertAction.init(title: "Remove Picture", style: .default, handler: { _ in
                action?()
            }))
        }
        
        actionSheetController.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { _ in
            self.openImagePicker(sourceType: .camera, forMedia: ["public.image"])
        }))
        
        actionSheetController.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { _ in
            self.openImagePicker(sourceType: .photoLibrary, forMedia: ["public.image"])
        }))
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func openImagePicker(sourceType: UIImagePickerController.SourceType, forMedia: [String]) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.sourceType = sourceType
        picker.delegate = self
        picker.mediaTypes = forMedia
        self.present(picker, animated: true, completion: nil)
    }       
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            
            if picker.sourceType == .camera {
                DemoUtils.save(chosenImage, completionBlock: { (success, url, error) in
                    if success {
                        let model = MediaModel(isImg: true, url: url, thumb: chosenImage)
                        didfetchMedia?(picker, model)
                    }
                })
                
            } else {
                let model = MediaModel(isImg: true, url: info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.imageURL)] as? URL, thumb: chosenImage)
                didfetchMedia?(picker, model)
            }
            
        } else {
            //selected media is video
            if let selectedVideo = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
                
                let asset = AVURLAsset(url: selectedVideo)
                let duration = CMTimeGetSeconds(asset.duration)
                if Int(duration) > ConstantModel.shared.validVideoDuration {
                    picker.dismiss(animated: false) {
                            self.showAlert(alertMessage: ConstantModel.shared.message.videoDurationError, buttonTitles: ["OK"], style: .alert, action: nil)
                    }
                } else {
                    
                    let generator = AVAssetImageGenerator(asset: asset)
                    //Get the 1st frame 3 seconds in
                    let cgImage = try! generator.copyCGImage(at: CMTimeMake(value: 3, timescale: 1), actualTime: nil)
                    let uiImage = UIImage(cgImage: cgImage)

                    let model = MediaModel(isImg: false, url: selectedVideo, thumb: uiImage)
                    didfetchMedia?(picker, model)
                }
            }
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
