//
//  ExtendedDateFormatter.swift
//  Tethers
//
//  Created by Pooja Rana on 26/12/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import Foundation

extension Date {
    
    func getDatefor(format: String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: self)
    }
    
    func getElapsedInterval() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: self, to: Date())

        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
            
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
            
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
            
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" :
                "\(hour)" + " " + "hours ago"
            
        } else if let minute = interval.minute, minute > 0 {
            return "\(minute)" + " " + "min ago"
            
        } else {
            return "Just now"
        }
    }
    
    func getDetailedElapseTime() -> String {
        let calendar = Calendar.current
        if calendar.isDateInYesterday(self) { return "YESTERDAY" }
        else if calendar.isDateInToday(self) { return "TODAY" }
        else if calendar.isDateInTomorrow(self) { return "TOMORROW" }
        else { return self.getElapsedInterval().localizedUppercase }
    }
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).localizedUppercase
    }
}

extension String {
    func getDatefor(format: String) -> Date {
        let df = DateFormatter()
        df.dateFormat = format
        return df.date(from: self)!
    }
}
