//
//  ColorExtension.swift
//  Tethers
//
//  Created by Pooja Rana on 06/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    open class var AppThemeColor: UIColor { return UIColor(red: 233/255.0, green: 60/255.0, blue: 92/255.0, alpha: 1) } 
    
    open class var cyanColor: UIColor  { return UIColor(red: 0/255.0, green: 184/255.0, blue: 162/255.0, alpha: 1) }
    open class var dividerColor: UIColor  { return UIColor(red: 239.0/255.0, green: 240.0/255.0, blue: 241.0/255.0, alpha: 1) }
    open class var textfieldDividerColor: UIColor  { return UIColor(red: 142.0/255.0, green: 141.0/255.0, blue: 157.0/255.0, alpha: 1) }
    open class var tethersCyan: UIColor  { return UIColor(red: 227/255, green: 245/255, blue: 249/255, alpha: 1.0) }
    
}
