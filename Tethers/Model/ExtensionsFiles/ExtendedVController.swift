//
//  ViewControllerExtensions.swift
//  Tethers
//
//  Created by Signity on 24/04/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import Photos

extension UIViewController {
        
    func showAlert(alertMessage: String?, buttonTitles: [String],  style: UIAlertController.Style, action: ((String?) -> Void)?) {
        
        let alert = UIAlertController(title: ConstantModel.shared.APP_NAME, message: alertMessage, preferredStyle: style)
        for buttonTitle in buttonTitles {
            alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler : {(alert: UIAlertAction!) in
                action?(buttonTitle)
            }))
        }
        
        if style == .actionSheet {
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler : nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func unwindToPrevious(_ sender: UIStoryboardSegue) { }

    func checkLibraryReadPermission(_ authorized: (() -> Void)?) {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos != .authorized {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    authorized?()
                } else {
                    self.showAlert(alertMessage: ConstantModel.shared.message.mediaLimitError, buttonTitles: ["OK"], style: .alert, action: nil)
                }
            })
        } else {
            self.showAlert(alertMessage: ConstantModel.shared.message.mediaLimitError, buttonTitles: ["OK"], style: .alert, action: nil)
        }
    }
    
    func alertPromptToAllowCameraAccessViaSetting(accessType: String) {
        let alert = UIAlertController(title: "Access to \(accessType) is restricted", message: "You need to enable access to \(accessType). Apple Settings > Privacy > \(accessType).", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = UIColor.cyanColor

        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel) { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        })
        present(alert, animated: true)
    }
    
    func showPopIfLocationServiceIsDisable() {
    
        let alert = UIAlertController(title: "Access to GPS is restricted", message: "GPS access is restricted. Show events by location, please enable GPS in the Settings > Privacy > Location Services.", preferredStyle: UIAlertController.Style.alert)
        alert.view.tintColor = UIColor.AppThemeColor

        alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) in
            print("")
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }))
        present(alert, animated: true)
    }
    
    func isModal() -> Bool {
        if (self.presentingViewController != nil) {
            return true
            
        } else if self.navigationController?.presentingViewController?.presentedViewController === self.navigationController {
            return true
            
        } else if self.tabBarController?.presentingViewController is UITabBarController {
            return true
            
        } else {
            return false
        }
    }
    
    func customPushPop(_ viewControllerToPresent: UIViewController?, ispush: Bool) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = convertToOptionalCATransitionSubtype(ispush ? CATransitionSubtype.fromLeft.rawValue : CATransitionSubtype.fromRight.rawValue)
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        ispush ? present(viewControllerToPresent!, animated: false) : dismiss(animated: false)
    }
    
    
    // custom zooming logic for zooming image.
    func performZoomInForStartingImageView(_ startingImageView: UIImageView) {
        
        startingImageView.isHidden = true
        
        let startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        var blackBackgroundView = UIView()
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.backgroundColor = UIColor.red
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        let gesture = ZoomGesture(target: self, action: #selector(handleZoomOut(_:)))
        zoomingImageView.addGestureRecognizer(gesture)
        gesture.originalImg = startingImageView
        
        if let keyWindow = UIApplication.shared.keyWindow {
            blackBackgroundView = UIView(frame: keyWindow.frame)
            blackBackgroundView.backgroundColor = UIColor.black
            blackBackgroundView.alpha = 0
            blackBackgroundView.tag = 1
            keyWindow.addSubview(blackBackgroundView)
            
            keyWindow.addSubview(zoomingImageView)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                blackBackgroundView.alpha = 1
                self.view.alpha = 0
                
                // math?
                // h2 / w1 = h1 / w1
                // h2 = h1 / w1 * w1
                let height = startingFrame!.height / startingFrame!.width * keyWindow.frame.width
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.center = keyWindow.center
            }, completion: nil)
        }
 
    }
    
    //Handle zoom out.
    @objc func handleZoomOut(_ tapGesture: ZoomGesture) {
        
        if let zoomOutImageView = tapGesture.view {
            //need to animate back out to controller
            zoomOutImageView.layer.cornerRadius = 16
            zoomOutImageView.clipsToBounds = true
            
            var blackBackgroundView: UIView?
            if let keyWindow = UIApplication.shared.keyWindow { blackBackgroundView = keyWindow.viewWithTag(1) }
            let startingFrame = tapGesture.originalImg.superview?.convert(tapGesture.originalImg.frame, to: nil)

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                zoomOutImageView.frame = startingFrame!
                blackBackgroundView?.alpha = 0
                self.view.alpha = 1
                
            }, completion: { (completed) in
                blackBackgroundView?.removeFromSuperview()
                zoomOutImageView.removeFromSuperview()
                tapGesture.originalImg.isHidden = false
            })
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalCATransitionSubtype(_ input: String?) -> CATransitionSubtype? {
	guard let input = input else { return nil }
	return CATransitionSubtype(rawValue: input)
}

class ZoomGesture: UITapGestureRecognizer {
    var originalImg: UIImageView!
}
