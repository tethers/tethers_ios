//
//  AWSUploader.swift
//  Tethers
//
//  Created by Pooja Rana on 23/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import AWSS3

typealias UploadCompletion = ((Bool, String) -> Void)?

class AWSUploader: NSObject {
    
    struct AWS {
        static let awsAccessKey = "AKIAIP5LUHZONCIQC2UQ"
        static let awsSecretKey = "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"
        static let bucketName   = "tethers"
        static let uploadPath   = "https://s3-ap-southeast-2.amazonaws.com/"
    }

    class func uploadMediaToAWS(_ model: MediaModel, uploadFinish: UploadCompletion) {
        let fileName = model.isImage ? EventManager.getNewImageName() : EventManager.getNewVideoName()

        uploadToAWS(fileName: fileName, filePath: model.mediaUrl!, type: (model.isImage ? "image/jpg" : "movie/mov")) { (status, info) in
            EventManager.clearFile(file: model.mediaUrl!)
            uploadFinish!(status, info)
        }
    }
        
    class func uploadToAWS(fileName: String, filePath: URL, type: String, uploadFinish: UploadCompletion)  {
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = AWS.bucketName
        uploadRequest?.key = fileName
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicRead
        uploadRequest?.body = filePath
        uploadRequest?.contentType = type
        AWSS3TransferManager.default().upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (awsTask: AWSTask<AnyObject>) -> Any? in
                
                if ((awsTask.error) != nil) {
                    uploadFinish!(false, awsTask.error.debugDescription)
                    
                } else {
                    uploadFinish!(true, (AWS.uploadPath+AWS.bucketName+"/"+fileName) as String)
                }
                return nil
        })
    }
}
