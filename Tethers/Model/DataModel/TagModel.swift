//
//  TagModel.swift
//  Tethers
//
//  Created by Pooja Rana on 26/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

public class TagModel: NSObject {
    var status: String = ""
    var name: String = ""
    var tagId: String = ""
    
    init(tagInfo: [String : Any]) {
        self.status     = tagInfo["status"] as? String ?? ""
        self.name       = tagInfo["name"] as? String ?? ""
        self.tagId      = tagInfo["id"] as? String ?? tagInfo["_id"] as? String ?? ""
    }
}
