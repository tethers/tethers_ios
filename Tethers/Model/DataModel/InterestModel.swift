//
//  InterestModel.swift
//  Tethers
//
//  Created by Pooja Rana on 07/08/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class InterestModel: NSObject {
    var name: String = ""
    var isSelected: Bool = false

    override init() {
        
    }
  
    init(interest: String) {
        name        = interest
        isSelected  = false
    }
    
    init(interest: String, selected: Bool) {
        name        = interest
        isSelected  = selected
    }
}

class JoinedInterestedModel: NSObject {
    var __id     = ""
    var type     = ""

    var postId   = ""
    var userId   = ""

    init(dict: [String: Any]) {
        self.__id      = dict["_id"] as? String ?? ""
        self.type      = dict["type"] as? String ?? ""
        self.postId    = dict["post_id"] as? String ?? ""
        self.userId    = dict["user_id"] as? String ?? ""
    }
}
