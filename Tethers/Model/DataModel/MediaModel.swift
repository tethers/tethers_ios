//
//  MediaModel.swift
//  Tethers
//
//  Created by Pooja Rana on 16/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class MediaModel: NSObject {
    
    var mediaId = ""
    var postId = ""
    var mediaViewCount = 0

    var isImage = true
    var mediaUrl: URL?
    var thumb: UIImage?
    var caption: String = ""
    var uploadedUrl: String = ""
    
    override init() {
        
    }
    
    init(isImg: Bool, url: URL?, thumb: UIImage?) {
        self.isImage     = isImg
        self.mediaUrl    = url
        self.thumb       = thumb
        self.caption     = ""
        self.uploadedUrl = ""
    }
}
