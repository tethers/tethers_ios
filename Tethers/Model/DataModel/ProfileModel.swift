//
//  ProfileModel.swift
//  Tethers
//
//  Created by Pooja Rana on 23/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ProfileModel: NSObject, NSCoding {
    
    var profile_id:String?
    var user_type:String = ""   //1: user, 2: admin, 3: organizer
    var account_type:String = "" //1: social, 2: signup

    var profilePic_url:String?
    var coverPic_url:String?

    var display_name:String = ""
    var dob:String?
    var email:String?

    var address: AddressModel?
    
    var followed = false
    
    var followersCount = 0
    var followingCount = 0
    
    override init() {
        
    }
    
    init(dict: [String: Any]) {
        self.profile_id     = dict["_id"] as? String ?? dict["id"] as? String
        self.user_type      = dict["user_type"] as? String ?? ""
        self.account_type   = dict["account_type"] as? String ?? ""

        self.profilePic_url = dict["image_url"] as? String
        self.coverPic_url   = dict["cover_picture"] as? String
        self.email          = dict["email"] as? String
        self.display_name   = dict["display_name"] as? String ?? ""
        
        self.dob            = dict["dob"] as? String
        
        if let location     = dict["address"] as? String {
            self.address    = AddressModel(location: ["location": location, "latitude": dict["Lat"] as? String ?? "0.0", "longitude": dict["Long"] as? String ?? "0.0"])
        }
        
        self.followersCount    = dict["userFollowers"] as? Int ?? 0
        self.followingCount    = dict["userFollowing"] as? Int ?? 0
        self.followed          = dict["userFollowing"] as? Int == 1 ? true : false
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        var data:[String: Any] = ["_id": aDecoder.decodeObject(forKey: "profile_id") as! String]

        data["user_type"]           = aDecoder.decodeObject(forKey: "user_type") as! String
        data["account_type"]        = aDecoder.decodeObject(forKey: "account_type") as! String

        data["image_url"]           = aDecoder.decodeObject(forKey: "profilePic_url") as? String
        data["cover_picture"]       = aDecoder.decodeObject(forKey: "coverPic_url") as? String
        
        data["email"]               = aDecoder.decodeObject(forKey: "email") as! String
        data["display_name"]        = aDecoder.decodeObject(forKey: "display_name") as! String
        
        data["dob"]                 = aDecoder.decodeObject(forKey: "dob") as? String
        self.init(dict: data)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(profile_id, forKey: "profile_id")
        aCoder.encode(user_type, forKey: "user_type")
        aCoder.encode(account_type, forKey: "account_type")

        aCoder.encode(profilePic_url, forKey: "profilePic_url")
        aCoder.encode(coverPic_url, forKey: "coverPic_url")
        
        aCoder.encode(email, forKey: "email")
        aCoder.encode(display_name, forKey: "display_name")
        
        aCoder.encode(dob, forKey: "dob")
    }
}
