//
//  PostModel.swift
//  Tethers
//
//  Created by Pooja Rana on 25/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class PostModel: NSObject {
    var postId: String = ""
    var postCreateDate: Date?
    var postType: String = ""

    var visibility: String = "Public"

    var postTitle: String = ""
    var postDescription: String = ""

    var taggedLocation: AddressModel?
    var taggedPeople: [String]?
    var tags: [TagModel]?

    var media: [MediaModel]?
    
    var ageCategory: String = ""
    var canJoin:Bool = true
    var allowCommentLike:Bool = true

    var totalLikes: Int = 0
    var userLiked:Bool = false

    var totalComment: Int = 0
    var userJoined:Bool = false
    var userInterested:Bool = false

    var postCreator:ProfileModel?
    
    var eventDate: String = ""
    var eventTime: String = ""
    
    var joinedInteresed: [JoinedInterestedModel]?
    
    var current_lat: String = ""
    var current_long: String = ""
}

class AddressModel: NSObject {
    var address: String = ""
    var shortAddress: String = ""

    var lat: String = ""
    var lng: String = ""
    
    override init() {
        
    }
    
    init(location: [String: Any]) {
        self.address = location["location"] as? String ?? ""
        self.shortAddress = location["shortLocation"] as? String ?? ""

        self.lat     = location["latitude"] as? String ?? "0.0"
        self.lng     = location["longitude"] as? String ?? "0.0"
    }
}

class LikeCommentModel: NSObject {
    var likeCommentId: String = ""
    var comment: String = ""
    var createDate: Date?
    
    var user: ProfileModel?
    
    override init() {
        
    }
    
    init(init dict: [String: Any]) {
        self.likeCommentId  = dict["_id"] as? String ?? ""
        self.comment        = dict["content"] as? String ?? ""
        if let createDate   = dict["created_at"] as? String {
            self.createDate = createDate.getDatefor(format: DateFormats.postDateFormat)
        }
        
        let userDict    = dict["user_id"] as? [String: Any]
        self.user = ProfileModel(dict: userDict!)
    }
}





