//
//  AutoCompletePlaceExtension.swift
//  Tethers
//
//  Created by Pooja Rana on 18/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit
import GooglePlaces

var locationCallback : ((_ completeAddress: AddressModel) ->())?

extension UIViewController: GMSAutocompleteViewControllerDelegate {
   
    func openPlaces() {
        let autocompleteController      = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    public func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        var locality                    = ""
        var country                     = ""

        for component in place.addressComponents! {
            switch component.type {
            case "country":
                country = component.name
            case "locality":
                locality = component.name
            default:
                break
            }
        }
        
        let shortAddress = locality == "" ? country : locality
        locationCallback!(AddressModel(location: ["location": place.formattedAddress!, "shortLocation": shortAddress,  "latitude": String(format: "%.5f", place.coordinate.latitude), "longitude": String(format: "%.5f", place.coordinate.longitude)]))
        dismiss(animated: true, completion: nil)
    }
    
    public func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    public func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    public func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    public func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
