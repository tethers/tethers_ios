//
//  ApiCallModel.swift
//  Tethers
//
//  Created by Signity on 05/06/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit

typealias SuccessHandler = ([String: Any]?) -> Void
typealias FailureHandler = ((String?) -> Void)?

class ApiCallModel: NSObject {
    
    static let shared = ApiCallModel()
    private override init() { }

    func apiCall(isSilent: Bool, url: String, parameters: [String: Any]?, type: String, delegate: UIViewController, success: @escaping SuccessHandler, failure: FailureHandler) {
        if EventManager.checkForInternetConnection() {
            
            if isSilent == false { EventManager.showloader() }
            WebService().uploadOrFetchData(paramRequest: parameters, baseURL: ConstantModel.shared.api.baseURL + url, method: type, withCompletion: { response in
                
                if response["status"] != nil && (response["status"] as? Bool)! {
                    success(response)
                } else {
                    DispatchQueue.main.async {
                        EventManager.hideloader()
                        failure?(response["message"] as? String)
                        if response["tokenError"] != nil && (response["tokenError"] as? Bool)! == true {
                            DispatchQueue.main.async {
                                //Remove data and LogOut & Navigate to app landing
                                UserDefaultsHandler.pageToShow = ""
                                NavigationManager.shared.checkWhereToNav()
                                ConstantModel.shared.appDelegate.window?.rootViewController?.showAlert(alertMessage: response["message"] as? String ?? ConstantModel.shared.message.TokenError, buttonTitles: ["Ok"], style: .alert, action: nil)
                            }
                        }
                    }
                }
            }, failure: { error in
                DispatchQueue.main.async {
                    EventManager.hideloader()
                    failure?(error.localizedDescription)
                    
                }
            })
        } else {
            delegate.showAlert(alertMessage: ConstantModel.shared.message.NoInternet, buttonTitles: ["Ok"], style: .alert, action: nil)
        }
    }
}
