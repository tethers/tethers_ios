//
//  WebService.swift
//  Tethers
//
//  Created by Signity on 4/27/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
import Foundation

class WebService: NSObject {
    var completion: (([String: Any]) -> Void)?
    var failure: ((Error) -> Void)?
    
    internal  func uploadOrFetchData(paramRequest: [String: Any]?, baseURL urlString: String, method methodName: String, withCompletion completionBlock: (([String: Any]) -> Void)?, failure failureBlock: ((Error) -> Void)?) {
        self.completion = completionBlock
        self.failure    = failureBlock
        do {
            var urlRequest = URLRequest(url: URL(string: urlString)!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 20)
            if UserDefaultsHandler.token != nil {
                urlRequest.setValue(UserDefaultsHandler.token, forHTTPHeaderField: "x-access-token")
            }
            urlRequest.httpMethod = methodName
            var paramData: Data!
            if paramRequest != nil {
                paramData = try JSONSerialization.data(withJSONObject: paramRequest as Any, options: .prettyPrinted)
            }
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = paramData
            
            self.getDataByRequest(urlRequest)
            
        } catch {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.completion!(["success": false, "description": ConstantModel.shared.message.NoInternet] as [String: Any])
        }
    }
    
    func getDataByRequest(_ request: URLRequest) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let _dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if error?._code == -1001 || error?.localizedDescription == "The request timed out." {
                self.failure!((error! as Error?)!)
            } else if error?.localizedDescription != "cancelled" {
                if error == nil {
                    self.getData(data!)
                } else {
                    self.failure!((error! as Error?)!)
                }
            } else {
                self.failure!((error! as Error?)!)
            }
        })
        _dataTask.resume()
    }
    
    fileprivate func getData(_ data: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            print(json!)
            self.completion!(json!)
        } catch let error as NSError {
            self.failure!(error)
        }
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
