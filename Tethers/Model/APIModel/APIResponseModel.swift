//
//  APIResponseModel.swift
//  Tambola Bingo
//
//  Created by Signity on 14/06/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit

typealias CallbackHandler = ((Any?) -> Void)?

class APIResponseModel: NSObject {
    
    static let shared = APIResponseModel()
    private override init() { }

    // MARK: -  Login/Logout 
    func parseLoginSignupResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let userData = response?["data"] as? [String: Any]
        UserDefaultsHandler.token    = userData!["token"] as? String ?? ""
        UserDefaultsHandler.userInfo = ProfileModel(dict: userData!)
        
        self.performUIChanges(sender) { _ in
            callback!(userData)
        }
    }
    
    // MARK: -  Profile 
    func parseGetProfileResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let userData  = response?["data"] as? [String: Any]
        let model = ProfileModel(dict: userData!)
        
        let createdPosts      = userData!["userposts"] as? [[String: Any]]
        let createdPostModels = ResponseConvertor.shared.myPostConvertor(createdPosts!)
        
        self.performUIChanges(sender) { _ in
            callback!(["profile": model, "createdPosts": createdPostModels])
        }
    }
    
    func parseGetJoiningPostsResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let joinedPosts  = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.joiningPostConvertor(joinedPosts!)
        
        self.performUIChanges(sender) { _ in
            callback!(models)
        }
    }
    
    func parseUpdateProfileResponse(model: ProfileModel, sender: UIViewController, callback: CallbackHandler) {
        UserDefaultsHandler.userInfo = model
        self.performUIChanges(sender) { _ in
            callback!(nil)
        }
    }
    
    func parseFollowerFollowings(response: [String: Any]?, isFollowers: Bool, sender: UIViewController, callback: CallbackHandler) {
        let list = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.followerFollowingConvertor(list!, isFollowers: isFollowers)
        
        self.performUIChanges(sender) { _ in
            callback!(models)
        }
    }
    
    func parseGetProfiles(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let list = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.userProfileConvertor(list!)
        
        self.performUIChanges(sender) { _ in
            callback!(models)
        }
    }
    
    // MARK: -  Post 
    func parseGetTagsResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let tagData = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.tagConvertor(tagData!)
        self.performUIChanges(sender) { _ in
            callback!(models)
        }
    }
    
    func parseGetPostsResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let postData = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.feedPostConvertor(postData!)
        self.performUIChanges(sender) { _ in
            callback!(models)
        }
    }
    
    func parseGetInterestResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler){
        let postData = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.getInterestConvertor(postData!)
        var sorted = models.sorted(by: { $0.name < $1.name })
        sorted.forEach { (model) in
            if model.name == "More"{
                let index = sorted.firstIndex(of: model)
                sorted.remove(at: index!)
                sorted.insert(model, at: sorted.count)
            }
        }
        self.performUIChanges(sender) { _ in
            callback!(sorted)
        }
    }
    
    func parseLikesComments(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        let likesData = response?["data"] as? [[String: Any]]
        let models = ResponseConvertor.shared.likesConvertor(likesData!)
        self.performUIChanges(sender) { _ in
            callback!(models)
        }
    }
    
    // MARK: -  Common 
    func parseCommonResponse(response: [String: Any]?, sender: UIViewController, callback: CallbackHandler) {
        self.performUIChanges(sender) { _ in
            callback!(response)
        }
    }
    
    // MARK: -   Perform UI Changes 
    func performUIChanges(_ sender: UIViewController, callback: CallbackHandler) {
        let when = DispatchTime.now() + 0.15
        DispatchQueue.main.asyncAfter(deadline: when) {
            EventManager.hideloader()
            callback!(nil)
        }
    }
}
