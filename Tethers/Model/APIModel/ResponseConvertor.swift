//
//  ResponseConvertor.swift
//  Tethers
//
//  Created by Pooja Rana on 24/10/18.
//  Copyright © 2018 Signity. All rights reserved.
//

import UIKit

class ResponseConvertor: NSObject {
    
    static let shared = ResponseConvertor()
    private override init() { }   
    
    // MARK: -  Profile 
    func userProfileConvertor(_ profileData: [[String: Any]]) -> [ProfileModel] {
        var userModelArr = [ProfileModel]()
        for profile in profileData {
            userModelArr.append(ProfileModel(dict: profile))
        }
        return userModelArr
    }
    
    func followerFollowingConvertor(_ profileData: [[String: Any]], isFollowers: Bool) -> [ProfileModel] {
        let parseKey = isFollowers ? "userFollowers" : "userFollowings"
        var followerFollowingArr = [ProfileModel]()
        for profile in profileData {
            if let dict = profile[parseKey] as? [String: Any] {
                let model = ProfileModel(dict: dict)
                
                if let userFollowed    = profile["userFollowing"] as? Int {
                    model.followed          = userFollowed == 1 ? true : false
                    
                } else {
                    if let userFollowed  = profile["userFollowing"] as? [[String: Any]] {
                        model.followed    = userFollowed.count > 0 ? true : false
                        
                    } else {
                        model.followed    = true
                    }
                }
                followerFollowingArr.append(model)
            }
        }
        return followerFollowingArr
    }
    
    // MARK: -  Tag 
    func tagConvertor(_ tagData: [[String: Any]]) -> [TagModel] {
        var tagModelArr = [TagModel]()
        for tag in tagData {
            tagModelArr.append(TagModel(tagInfo: tag))
        }
        return tagModelArr
    }

    
    func getInterestConvertor(_ interestData: [[String: Any]]) -> [InterestModel]{
        var interestModelArr = [InterestModel]()
        for post in interestData {
            let interests    = post["interest"] as? [[String:Any]]
            let interestInfo = interests![0]
            let allKeys = interestInfo.keys
            for key in allKeys {
                let model            = InterestModel()
                model.name           = key
                model.isSelected     = (interestInfo[key] as! String) == "0" ? false : true
                interestModelArr.append(model)
            }
        }
        return interestModelArr
    }
    
    // MARK: -  Post 
    func feedPostConvertor(_ postData: [[String: Any]]) -> [PostModel] {
        
        var postModelArr = [PostModel]()
        for post in postData {
            let postInfo     = post["combinepost"] as? [String: Any]
            let model        = parsePost(postInfo!)
            
            if let tagArr = post["posttags"] as? [[String:Any]] {
                model.tags           = [TagModel]()
                for tagDict in tagArr {
                    model.tags?.append(TagModel(tagInfo: tagDict))
                }
            }
            
            if let userInfoArr = post["userdata"] as? [[String: Any]] {
                model.postCreator = ProfileModel(dict: userInfoArr[0])
            }
            
            if let joinedInterested = post["postjoininterestsdata"] as? [[String: Any]] {
                model.joinedInteresed = [JoinedInterestedModel]()
                for dict in joinedInterested {
                    let peopleJoined = JoinedInterestedModel(dict: dict)
                    if UserDefaultsHandler.userInfo?.profile_id == peopleJoined.userId {
                        peopleJoined.type == "join" ? (model.userJoined = true) : (model.userInterested = true)
                    }
                    model.joinedInteresed?.append(JoinedInterestedModel(dict: dict))
                }
            }
            postModelArr.append(model)
        }
        return postModelArr
    }
    
    func myPostConvertor(_ postData: [[String: Any]]) -> [PostModel] {
        var postModelArr = [PostModel]()
        for post in postData {
            postModelArr.append(parsePost(post))
        }
        return postModelArr
    }
    
    func joiningPostConvertor(_ postData: [[String: Any]]) -> [PostModel] {
        var postModelArr = [PostModel]()
        for post in postData {
            let postInfo = post["post_id"] as? [String: Any]
            postModelArr.append(parsePost(postInfo!))
        }
        return postModelArr
    }
    
    func parsePost(_ postInfo: [String: Any]) -> PostModel {
        let model            = PostModel()
        model.postId         = postInfo["_id"] as? String ?? ""
        
        if let createDate    = postInfo["created_at"] as? String {
            model.postCreateDate = createDate.getDatefor(format: DateFormats.postDateFormat)
        }
        
        model.canJoin        = postInfo["isAvailableForJoin"] as? String == "1" ? true : false
        model.postTitle      = postInfo["postTitle"] as? String ?? ""
        model.visibility     = postInfo["visibility"] as? String ?? ""
        
        if let mediaArr = postInfo["media"] as? [[String:Any]] {
            model.media           = [MediaModel]()
            for mediaJson in mediaArr {
                let mediaModel = MediaModel(isImg: mediaJson["media_type"] as? String == "photo" ? true : false, url: nil, thumb: nil)
                mediaModel.mediaId = mediaJson["_id"] as? String ?? ""
                mediaModel.postId = mediaJson["post_id"] as? String ?? ""
                mediaModel.mediaViewCount = mediaJson["viewscount"] as? Int ?? 0
                mediaModel.caption = mediaJson["caption"] as? String ?? ""
                mediaModel.uploadedUrl = mediaJson["url"] as? String ?? ""
                model.media?.append(mediaModel)
            }
        }
        
        if (postInfo["location"] as? String) != nil {
            model.taggedLocation = AddressModel(location: postInfo)
        }
        
        model.totalLikes     = postInfo["likeTotal"] as? Int ?? 0
        model.totalComment   = postInfo["commentTotal"] as? Int ?? 0
        
        if let userLikeOrNot = postInfo["userlikeornot"] as? [[String:Any]] {
            model.userLiked      = userLikeOrNot.count > 0 ? true : false
        }
        
        model.ageCategory      = postInfo["agecategory"] as? String ?? ""
        model.postDescription  = postInfo["description"] as? String ?? ""
        model.postType         = postInfo["post_type"] as? String ?? ""
        
        model.eventDate          = postInfo["event_date"] as? String ?? ""
        model.eventTime          = postInfo["event_time"] as? String ?? ""
        model.allowCommentLike   = postInfo["allowcommentlike"] as? Int == 0 ? false : true
        
        var currentLoc = postInfo["current_loc"] as? [String:Any]
        if currentLoc != nil{
            var obj = currentLoc?["coordinates"] as? [Any]
            model.current_lat  = String(describing: obj![1])
            model.current_long = String(describing: obj![0])
        }
        
        return model
    }
    
    func likesConvertor(_ data: [[String: Any]]) -> [LikeCommentModel] {
        
        var modelArr = [LikeCommentModel]()
        for dict in data {
            modelArr.append(LikeCommentModel(init: dict))
        }
        return modelArr
    }
}
