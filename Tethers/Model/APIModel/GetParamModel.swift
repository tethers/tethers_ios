//
//  GetParamModel.swift
//  Tethers
//
//  Created by Signity on 26/05/17.
//  Copyright © 2017 signity solution. All rights reserved.
//

import UIKit
import CoreLocation

class GetParamModel: NSObject {

    static let shared = GetParamModel()
    private override init() { }

    // MARK: -  Login/Logout 
    func getSignUpParam(_ name: String, _ account: String, _ role: String, _ email: String?, _ password: String, _ provider: String, _ oauthId: String?) -> [String: Any]? {
        
        var param = ["display_name": name,
                     "account_type": account,
                     "user_type": role,
                     "email": email ?? "",
                     "password": password,
                     "image_url": "",
                     "dob": "",
                     "oauth_provider": provider,
                     "gender": ""] as [String: Any]
        
        if oauthId != nil { param["oauth_id"] = oauthId }
        return param
    }
    
    func getLogInParam(_ email: String, _ password: String, _ role: String) -> [String: Any]? {
        // User Type admin, user, organizer
        let param = ["email":email,
                     "password": password,
                     "user_type": role] as [String: Any]
        return param
    }
    
    func getAddInterestParam(long: String, lat: String, interest: [InterestModel]) -> [String: Any]? {
        var interestDict = [String: Any]()
        
        interest.forEach { (model) in
            interestDict[model.name] = model.isSelected ? "1" : "0"
        }
        let param = ["latitude":lat ,
                     "longitude":long ,
                     "interest": interestDict] as [String: Any]
        return param
    }
    
    // MARK: -  User 
    func getUserParam(offset: Int) -> [String: Any]? {
        return ["user_type": UserDefaultsHandler.userInfo?.user_type ?? "",
                "skip": offset,
                "limit": ConstantModel.shared.pageLimit] as [String: Any]
    }
    
    func getUpdateUserParam(model: ProfileModel) -> [String: Any]? {
        var param = ["display_name": model.display_name] as [String: Any]
        
        if EventManager.isValidUrl(url: model.coverPic_url) {
            param["cover_picture"] = model.coverPic_url
        }
        
        if EventManager.isValidUrl(url: model.profilePic_url) {
            param["image_url"] = model.profilePic_url
        }
        
        if model.dob != nil {
            param["dob"] = model.dob
        }
        
        if model.address != nil {
            param["address"] = model.address?.address
            param["Lat"] = model.address?.lat
            param["Long"] = model.address?.lng
        }
        return param
    }
    
    func followUnFollowParam(_ userId: String) -> [String: Any] {
        return ["follow_id": userId]
    }

    func getOtherUserParam(_ userId: String, _ offset: Int) -> [String: Any] {
        return ["user_id": userId,
                "skip": offset,
                "limit": ConstantModel.shared.pageLimit]
    }
    
    func getRemovePicParam(type: String, name: String) -> [String: Any]? {
        let param = ["picture_Type":type,
                     "image_name":name] as [String: Any]
        return param
    }
    
    // MARK: -  Password 
    func getForgotPasswordParam(_ email: String) -> [String: Any]? {
        return ["email": email] as [String: Any]
    }
    
    func getChangePassParam(oldPassword: String, newPassword: String) -> [String: Any]? {
        let param = ["old_apssword": oldPassword ,
                     "new_password": newPassword ,
                     "user_type": (UserDefaultsHandler.userInfo?.user_type)!] as [String: Any]
        return param
    }
    
    // MARK: -  Post 
    func getListWithPagination(offset: Int) -> [String: Any]? {
        let param = ["skip": offset,
                     "limit": ConstantModel.shared.pageLimit] as [String: Any]
        return param
    }
    
    func getNearestEvents() -> [String: Any]?  {
        return ["lat": ConstantModel.shared.currentAddress.lat,
                "long": ConstantModel.shared.currentAddress.lng] as [String: Any]
    }
    
    func getUploadPostParam(model: PostModel) -> [String: Any]? {
        //Set Tags
        var selectedTags = [String]()
        for tag in model.tags! {
            selectedTags.append(tag.tagId)
        }
       
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: selectedTags, options: .prettyPrinted)
            let selectedTagsString = String(data: jsonData, encoding: .utf8)
            
            var mediaArr = [[String: String]]()
            for media in model.media! {
                let dict = ["caption": media.caption,
                            "url": media.uploadedUrl,
                            "media_type": media.isImage ? "photo" : "video"]
                mediaArr.append(dict)
            }
           
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: mediaArr, options: .prettyPrinted)
                let selectedMedia = String(data: jsonData, encoding: .utf8)
                
                var param = ["visibility": model.visibility,
                             "tags": selectedTagsString ?? "",
                             "postTitle": model.postTitle,
                             "media":selectedMedia ?? "",
                             "isAvailableForJoin": model.canJoin ? "1" : "0",
                             "agecategory": model.ageCategory,
                             "allowcommentlike": model.allowCommentLike ? "1" : "0",
                             "post_type": model.postType,
                             "description": model.postDescription,
                             "event_date": model.eventDate,
                             "event_time": model.eventTime,
                             "current_lat": model.current_lat,
                             "current_long": model.current_long,
                             "interest" : "Sport"] as [String: Any]
                
                
                if model.taggedLocation != nil  {
                    param["location"] = model.taggedLocation?.address ?? ""
                    param["latitude"] = model.taggedLocation?.lat ?? ""
                    param["longitude"] = model.taggedLocation?.lng ?? ""
                }
                return param
                
            } catch {
                print(error.localizedDescription)
                return nil
            }
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func postLikeParam(postId: String, likePressed: Bool) -> [String: Any]? {
        return ["post_id": postId,
                "status": likePressed ? "0" : "1"] as [String: Any]
    }
    
    func postCommentParam(postId: String, content: String) -> [String: Any]? {
        return ["post_id": postId,
                "content": content] as [String: Any]
    }
    
    func getLikesCommentsParam(postId: String) -> [String: Any]? {
        return ["post_id": postId] as [String: Any]
    }
    
    func joinInterestParam(postId: String, type: String) -> [String: Any]? {
        return ["post_id": postId,
                "type": type] as [String: Any]
    }
}
