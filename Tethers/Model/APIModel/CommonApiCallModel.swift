//
//  CommonApiCallModel.swift
//  Tethers
//
//  Created by Pooja Rana on 21/01/19.
//  Copyright © 2019 Signity. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func likePostAPI(param: [String: Any]) {
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.likePost, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in })
            
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
    
    func joinInterestAPI(param: [String: Any]) {
        ApiCallModel.shared.apiCall(isSilent: false, url:ConstantModel.shared.api.joinInterest, parameters: param, type: ConstantModel.shared.api.PostAPI, delegate: self, success: { response in
            APIResponseModel.shared.parseCommonResponse(response: response, sender: self, callback: { _ in })
        }, failure: { failureResponse in
            self.showAlert(alertMessage: failureResponse ?? ConstantModel.shared.message.ServiceFailure, buttonTitles: ["Ok"], style: .alert, action:nil)
        })
    }
}
